<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<nav>
    <a href="#">
        <img src="../../resources/img/student.png" width="30" height="30" class="d-inline-block align-top" alt="">
    </a>
    <a class="nav-item" href="/profile">Personal area</a>
    <a class="nav-item" href="/list/student/1">View students</a>
    <a class="nav-item" href="/list/request">View requests</a>
    <a class="nav-item" href="/list/speciality">View specialities</a>
    <a class="nav-item" href="/list/faculty">View faculties</a>
    <a class="nav-item" href="/logout">Log out</a>

</nav>
