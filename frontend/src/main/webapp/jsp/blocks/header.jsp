<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="../../resources/js/libs/jquery-3.2.1.min.js"></script>
<script src="../../resources/js/libs/popper.min.js"></script>
<script src="../../resources/js/libs/bootstrap.min.js"></script>
<link href="../../resources/css/libs/bootstrap.min.css" rel="stylesheet" type="text/css" media="all"/>
<link href="../../resources/css/general.css" rel="stylesheet" type="text/css" media="all"/>
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<body>
</body>