
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <jsp:include page="blocks/header.jsp" />
    <title>Welcome</title>
</head>
<body>

<div class="container-fluid wrapper">
    <div class="jumbotron yellow">

    <h2>
        Do the registration
    </h2>

    <form:form action="/submit/registration" modelAttribute="userViewModel" method="post">
        <div class="col-xs-12">
            <form:label path="email" class="sr-only" for="inlineFormInputGroup">Login</form:label>
            <form:input path="email" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Email"/>
            <p>
                <form:errors path="email" cssClass="error"/>
            </p>

            <form:label path="userPassword" class="sr-only" for="inlineFormInput">Password</form:label>
            <form:password path="userPassword" class="form-control margin" id="inlineFormInput" placeholder="Password"/>
            <p>
                <form:errors path="userPassword" cssClass="error"/>
            </p>

            <form:label path="role" class="sr-only">Role:</form:label>
            <form:select path="role" class=" margin form-control">
                <form:option value="ROLE_student">student</form:option>
                <form:option value="ROLE_admin">admin</form:option>
            </form:select>
            <p>
                <form:errors path="role" cssClass="error"/>
            </p>

            <button type="submit" class="btn btn-outline-warning">Registrate</button>
        </div>
    </form:form>
    </div>
</div>


<jsp:include page="blocks/footer.jsp">
    <jsp:param name="previousPage" value="${previousPage}" />
</jsp:include>

</body>
</html>
