<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <title>Login page</title>
</head>
<body>
<jsp:include page="blocks/header.jsp"/>


<div class="container-fluid wrapper">
    <div class="jumbotron yellow">

        <h2>
            Welcome to the web-practice manager!
        </h2>
        <p>
            Log in
        </p>

        <form action="/j_spring_security_check" name="form_login" method="post">
            <div class="col-xs-12">
                <p>
                    <c:if test="${error != null}">
                        ${error}
                    </c:if>
                </p>

                <div class="form-item">
                    <input name="username" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Email"/>
                </div>
                <div class="form-item">
                    <input type="password" name="password" class="form-control margin" id="inlineFormInput" placeholder="Password"/>
                </div>
                <div class="form-item">
                    <button name="submit" class="btn btn-outline-warning">Log in</button>
                </div>
                <div class="form-item">
                    <a href="/registration" class="do-registration">Do registration</a>
                </div>
            </div>
        </form>
    </div>
</div>



</body>
</html>
