<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <jsp:include page="../blocks/header.jsp" />
    <title>Something gone wrong</title>
</head>
<body>

<div class="container-fluid">

    <div class="jumbotron yellow">

        <h2>
            OOPS
        </h2>
        <hr>
        <p>The removal imposible</p>

        <p>
            <img src="../../resources/img/warning.png" width="70" height="70" class="d-inline-block align-top" alt="">
        </p>
    </div>
</div>

<jsp:include page="../blocks/footer.jsp">
    <jsp:param name="previousPage" value="${previousPage}" />
</jsp:include>

</body>
</html>
