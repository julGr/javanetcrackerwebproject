<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="../../blocks/header.jsp" />
    <title>Request</title>
</head>
<body>

<jsp:include page="../../blocks/navbar.jsp" />

<div class="container-fluid wrapper2">
    <div class="jumbotron yellow">

        <c:choose>
            <c:when test="${edited_request_id !=null}">

                <h2>
                    Edit request
                </h2>

                <form:form action="/request/submit/${edited_request_id}" modelAttribute="requestViewModel" method="post">
                    <div class="col-xs-12">
                        <form:label class="sr-only" for="inlineFormInputGroup" path="requestName">Request name:</form:label>
                        <form:input path="requestName" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Request name"/>
                        <p>
                            <form:errors path="requestName" cssClass="error"/>
                        </p>
                        <form:label class="sr-only" for="inlineFormInputGroup" path="dateFrom">Starts:</form:label>
                        <form:input path="dateFrom" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Starts"/>
                        <p>
                            <form:errors path="dateFrom" cssClass="error"/>
                        </p>
                        <form:label class="sr-only" for="inlineFormInputGroup"  path="dateTo">Ends:</form:label>
                        <form:input path="dateTo" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Ends"/>
                        <p>
                            <form:errors path="dateTo" cssClass="error"/>
                        </p>
                        <form:label for="inlineFormInputGroup"  path="availableSpecialtyName">Available speciality:</form:label>
                        <form:select path="availableSpecialtyName" type="text" class="margin form-control" id="inlineFormInputGroup" >
                            <c:forEach items="${allSpeciality}" var="speciality">
                                <form:option value="${speciality.specialtyName}">${speciality.specialtyName}</form:option>
                            </c:forEach>
                        </form:select>
                        <p>
                            <form:errors path="availableSpecialtyName" cssClass="error"/>
                        </p>
                    </div>
                    <input type="submit" class="btn btn-outline-warning" value="Edit">
                </form:form>
            </c:when>

            <c:otherwise>

                <h2>
                    Add request
                </h2>

                <form:form action="/request/submit" modelAttribute="requestViewModel" method="post">
                    <div class="col-xs-12">
                        <form:label class="sr-only" for="inlineFormInputGroup" path="requestName">Request name:</form:label>
                        <form:input path="requestName" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Request name"/>
                        <p>
                            <form:errors path="requestName" cssClass="error"/>
                        </p>
                        <form:label class="sr-only" for="inlineFormInputGroup" path="dateFrom">Starts:</form:label>
                        <form:input path="dateFrom" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Starts"/>
                        <p>
                            <form:errors path="dateFrom" cssClass="error"/>
                        </p>
                        <form:label class="sr-only" for="inlineFormInputGroup"  path="dateTo">Ends:</form:label>
                        <form:input path="dateTo" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Ends"/>
                        <p>
                            <form:errors path="dateTo" cssClass="error"/>
                        </p>
                        <form:label for="inlineFormInputGroup"  path="availableSpecialtyName">Available speciality:</form:label>
                        <form:select path="availableSpecialtyName" type="text" class="margin form-control" id="inlineFormInputGroup" >
                            <c:forEach items="${allSpeciality}" var="speciality">
                                <form:option value="${speciality.specialtyName}">${speciality.specialtyName}</form:option>
                            </c:forEach>
                        </form:select>
                        <p>
                            <form:errors path="availableSpecialtyName" cssClass="error"/>
                        </p>
                    </div>
                    <input type="submit" class="btn btn-outline-warning" value="Add request">
                </form:form>
            </c:otherwise>
        </c:choose>
    </div>
</div>

<jsp:include page="../../blocks/footer.jsp">
    <jsp:param name="previousPage" value="${previousPage}" />
</jsp:include>

</body>
</html>
