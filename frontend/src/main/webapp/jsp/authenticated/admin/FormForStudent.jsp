<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="../../blocks/header.jsp" />
    <title>Student</title>
</head>
<body>

<jsp:include page="../../blocks/navbar.jsp" />

<div class="container-fluid wrapper2">
    <div class="jumbotron yellow">
        <c:choose>
            <c:when test="${edited_student_id !=null}">
                <h2>
                    Edit student
                </h2>
                <form:form action="/student/submit/${edited_student_id}" modelAttribute="studentViewModel" method="post">
                    <div class="col-xs-12">
                        <form:label path="fName" class="sr-only" for="inlineFormInputGroup">Fitst name:</form:label>
                        <form:input path="fName" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="First name"/>
                        <p><form:errors path="fName" cssClass="error"/></p>

                        <form:label path="lName" class="sr-only" for="inlineFormInputGroup">Last name:</form:label>
                        <form:input path="lName" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Last name"/>
                        <p><form:errors path="lName" cssClass="error"/></p>

                        <form:label path="averageScore" for="inlineFormInputGroup">Average score:</form:label>
                        <form:input path="averageScore" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Average score"/>
                        <p><form:errors path="averageScore" cssClass="error"/></p>

                        <form:label path="stGroup" class="sr-only" for="inlineFormInputGroup">Student group:</form:label>
                        <form:input path="stGroup" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Student group"/>
                        <p><form:errors path="stGroup" cssClass="error"/></p>

                        <form:label path="specialtyName" for="inlineFormInputGroup">Speciality:</form:label>
                        <form:select path="specialtyName" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Speciality" >
                            <c:forEach items="${allSpeciality}" var="speciality">
                                <form:option value="${speciality.specialtyName}">${speciality.specialtyName}</form:option>
                            </c:forEach>
                        </form:select>
                        <p><form:errors path="specialtyName" cssClass="error"/></p>

                        <form:label path="isBudget" for="inlineFormInputGroup" >Is budget:</form:label>
                        <form:select path="isBudget" type="text" class="margin form-control" id="inlineFormInputGroup" >
                            <form:option value="yes">yes</form:option>
                            <form:option value="no">no</form:option>
                        </form:select>
                        <p><form:errors path="isBudget" cssClass="error"/></p>
                    </div>
                    <input type="submit" class="btn btn-outline-warning" value="Edit">
                </form:form>
            </c:when>

            <c:otherwise>

                <h2>
                    Add student
                </h2>
                <form:form action="/student/submit" modelAttribute="studentViewModel" method="post">
                    <div class="col-xs-12">
                        <form:label path="fName" class="sr-only" for="inlineFormInputGroup">Fitst name:</form:label>
                        <form:input path="fName" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="First name"/>
                        <p><form:errors path="fName" cssClass="error"/></p>

                        <form:label path="lName" class="sr-only" for="inlineFormInputGroup">Last name:</form:label>
                        <form:input path="lName" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Last name"/>
                        <p><form:errors path="lName" cssClass="error"/></p>

                        <form:label path="averageScore" for="inlineFormInputGroup">Average score:</form:label>
                        <form:input path="averageScore" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Average score"/>
                        <p><form:errors path="averageScore" cssClass="error"/></p>

                        <form:label path="stGroup" class="sr-only" for="inlineFormInputGroup">Student group:</form:label>
                        <form:input path="stGroup" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Student group"/>
                        <p><form:errors path="stGroup" cssClass="error"/></p>

                        <form:label path="specialtyName" for="inlineFormInputGroup">Speciality:</form:label>
                        <form:select path="specialtyName" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Speciality" >
                            <c:forEach items="${allSpeciality}" var="speciality">
                                <form:option value="${speciality.specialtyName}">${speciality.specialtyName}</form:option>
                            </c:forEach>
                        </form:select>
                        <p><form:errors path="specialtyName" cssClass="error"/></p>

                        <form:label path="isBudget" for="inlineFormInputGroup" >Is budget:</form:label>
                        <form:select path="isBudget" type="text" class="margin form-control" id="inlineFormInputGroup" >
                            <form:option value="yes">yes</form:option>
                            <form:option value="no">no</form:option>
                        </form:select>
                        <p><form:errors path="isBudget" cssClass="error"/></p>
                    </div>
                    <input type="submit" class="btn btn-outline-warning" value="Add">
                </form:form>

            </c:otherwise>
        </c:choose>
    </div>
</div>

<jsp:include page="../../blocks/footer.jsp">
    <jsp:param name="previousPage" value="${previousPage}" />
</jsp:include>

</body>
</html>
