<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <jsp:include page="../../blocks/header.jsp" />
    <title>Speciality</title>
</head>
<body>

<jsp:include page="../../blocks/navbar.jsp" />

<div class="container-fluid wrapper2">
    <div class="jumbotron yellow">
        <c:choose>
            <c:when test="${edited_speciality_id != null}">

                <h2>
                    Edit speciality
                </h2>

                <form:form action="/speciality/submit/${edited_speciality_id}" modelAttribute="specialityViewModel" method="post">
                    <div class="col-xs-12">
                        <form:label class="sr-only" for="inlineFormInputGroup" path="specialtyName">Speciality name:</form:label>
                        <form:input path="specialtyName" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Speciality name"/>
                        <p>
                            <form:errors path="specialtyName" cssClass="error"/>
                        </p>
                        <form:label path="facultyName" for="inlineFormInputGroup">Faculty:</form:label>
                        <form:select path="facultyName" type="text" class="margin form-control" id="inlineFormInputGroup" >
                            <c:forEach items="${allFaculties}" var="faculty">
                                <form:option value="${faculty.facultyName}">${faculty.facultyName}</form:option>--%>
                            </c:forEach>
                        </form:select>
                        <p>
                            <form:errors path="facultyName" cssClass="error"/>
                        </p>

                    </div>
                    <input type="submit" class="btn btn-outline-warning" value="Edit">
                </form:form>
            </c:when>
            <c:otherwise>

                <h2>
                    Add speciality
                </h2>
                
                <form:form action="/speciality/submit" modelAttribute="specialityViewModel" method="post">
                    <div class="col-xs-12">

                        <form:label class="sr-only" for="inlineFormInputGroup" path="specialtyName">Speciality name:</form:label>
                        <form:input path="specialtyName" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Speciality name"/>
                        <p>
                            <form:errors path="specialtyName" cssClass="error"/>
                        </p>
                        <form:label path="facultyName" for="inlineFormInputGroup">Faculty:</form:label>
                        <form:select path="facultyName" type="text" class="margin form-control" id="inlineFormInputGroup" >
                            <c:forEach items="${allFaculties}" var="faculty">
                                <form:option value="${faculty.facultyName}">${faculty.facultyName}</form:option>--%>
                            </c:forEach>
                        </form:select>
                        <p>
                            <form:errors path="facultyName" cssClass="error"/>
                        </p>
                    </div>
                    <input type="submit" class="btn btn-outline-warning" value="Add speciality">
                </form:form>
            </c:otherwise>
        </c:choose>
    </div>
</div>

<jsp:include page="../../blocks/footer.jsp">
    <jsp:param name="previousPage" value="${previousPage}" />
</jsp:include>

</body>
</html>
