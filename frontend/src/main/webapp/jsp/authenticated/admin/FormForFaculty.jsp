<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <jsp:include page="../../blocks/header.jsp" />
    <title>Faculty</title>
</head>
<body>

<jsp:include page="../../blocks/navbar.jsp" />

<div class="container-fluid wrapper2">
    <div class="jumbotron yellow">

        <c:choose>
            <c:when test="${edited_faculty_id !=null}">
                <h2>
                    Edit faculty
                </h2>
                <form:form action="/faculty/submit/${edited_faculty_id}" modelAttribute="facultyViewModel" method="post">
                    <div class="col-xs-12">
                        <form:label path="facultyName" class="sr-only" for="inlineFormInputGroup">Faculty name:</form:label>
                        <form:input value="${param.facultyName}" path="facultyName" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Faculty name"/>
                        <p>
                            <form:errors path="facultyName" cssClass="error"/>
                        </p>
                    </div>
                    <input type="submit"  class="btn btn-outline-warning" value="Edit">
                </form:form>
            </c:when>

            <c:otherwise>
                <h2>
                    Add faculty
                </h2>
                <form:form action="/faculty/submit" modelAttribute="facultyViewModel" method="post">
                    <div class="col-xs-12">
                        <form:label path="facultyName" class="sr-only" for="inlineFormInputGroup">Faculty name:</form:label>
                        <form:input value="${param.facultyName}" path="facultyName" type="text" class="margin form-control" id="inlineFormInputGroup" placeholder="Faculty name"/>
                        <p>
                            <form:errors path="facultyName" cssClass="error"/>
                        </p>
                    </div>
                    <input type="submit"  class="btn btn-outline-warning" value="Add">
                </form:form>
            </c:otherwise>
        </c:choose>

    </div>
</div>

<jsp:include page="../../blocks/footer.jsp">
    <jsp:param name="previousPage" value="${previousPage}" />
</jsp:include>

</body>
</html>