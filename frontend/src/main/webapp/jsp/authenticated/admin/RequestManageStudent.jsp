<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <jsp:include page="../../blocks/header.jsp" />
    <title>Practice manager</title>
</head>
<body>

<jsp:include page="../../blocks/navbar.jsp" />


<div class="info-table">
    <h2>
        Manage student on practice
    </h2>

        <div class="alert alert-warning">
            <H2>
                    ${requestViewModel.requestName}
            </H2>
            <hr>
            <p>
                Period:
                    ${requestViewModel.dateFrom} - ${requestViewModel.dateTo}
            </p>
            <p>
                Available speciality:
                    ${requestViewModel.availableSpecialtyName}
            </p>

                <div class="info-table">
                    <table class="table-bordered table table-hover">
                        <tr>
                            <th>First name:</th>
                            <th>Last name:</th>
                            <th>Speciality:</th>
                            <th>Group:</th>
                            <th>Is budget:</th>
                            <th>Average score:</th>
                            <th>Status:</th>
                            <th>Practice period:</th>
                            <th>Requests:</th>
                            <th></th>
                        </tr>
                        <c:forEach items="${allStudents}" var="student">
                            <tr>
                                <td>${student.fName}</td>
                                <td>${student.lName}</td>
                                <td>${student.specialtyName}</td>
                                <td>${student.stGroup}</td>
                                <td>${student.isBudget}</td>
                                <td>${student.averageScore}</td>
                                <td>${student.status}</td>
                                <td>${student.practicePeriod}</td>
                                <td>
                                    <c:forEach items="${student.requests}" var="request">
                                        ${request}
                                    </c:forEach>
                                </td>
                                <td>
                                    <c:choose>
                                        <c:when test="${!student.requests.contains(requestViewModel.requestName)}">
                                            <a type="submit" href="/request/manager/${requestViewModel.id}/${student.id}" class="badge badge-danger manage-student">Appoint</a>
                                        </c:when>
                                        <c:otherwise>
                                            <a type="submit" href="/request/manager/cancel/${requestViewModel.id}/${student.id}" class="badge badge-danger manage-student">Cancel</a>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
        </div>
</div>

<jsp:include page="../../blocks/footer.jsp">
    <jsp:param name="previousPage" value="${previousPage}" />
</jsp:include>

</body>
</html>
