<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <jsp:include page="../blocks/header.jsp" />
    <title>Student table</title>
</head>
<body>

<jsp:include page="../blocks/navbar.jsp" />

    <h1 class="jumbotron yellow">
        All information about students
    </h1>

<div class="info-table">

    <div class="custom-container">
        <div class="btn-group">
            <sec:authorize access="hasRole('ROLE_admin')">
                <a type="submit" href="/student/add" class="btn btn-outline-warning">Add student</a>
            </sec:authorize>
        </div>

        <div class="btn-group">
            <button type="button" class="btn btn-outline-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                All in speciality
            </button>
            <div class="dropdown-menu">
                <c:forEach items="${allSpecialities}" var="speciality">
                    <a class="dropdown-item" href="/list/filterBySpeciality/${speciality.id}">
                            ${speciality.specialtyName}
                    </a>
                </c:forEach>
            </div>
        </div>

        <div class="btn-group">
            <button type="button" class="btn btn-outline-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                All in faculty
            </button>
            <div class="dropdown-menu">
                <c:forEach items="${allFaculties}" var="faculty">
                    <a class="dropdown-item" href="/list/filterByFaculty/${faculty.id}">
                            ${faculty.facultyName}
                    </a>
                </c:forEach>
            </div>
        </div>
    </div>

    <table class="table-bordered table table-hover">
        <tr>
            <th>First name:</th>
            <th>Last name:</th>
            <th>Speciality:</th>
            <th>Faculty:</th>
            <th>Group:</th>
            <th>Is budget:</th>
            <th>Average score:</th>
            <th>Status:</th>
            <th>Practice period:</th>
            <th>Requests:</th>
            <sec:authorize access="hasRole('ROLE_admin')">
                <th>Action</th>
            </sec:authorize>
        </tr>
        <c:forEach items="${allStudents}" var="student">
            <tr>
                <td>${student.fName}</td>
                <td>${student.lName}</td>
                <td>${student.specialtyName}</td>
                <td>${student.facultyName}</td>
                <td>${student.stGroup}</td>
                <td>${student.isBudget}</td>
                <td>${student.averageScore}</td>
                <td>${student.status}</td>
                <td>${student.practicePeriod}</td>
                <td>
                    <c:forEach items="${student.requests}" var="request">
                        ${request}
                    </c:forEach>
                </td>

                <sec:authorize access="hasRole('ROLE_admin')">
                    <th>
                        <a href="/student/delete/${student.id}">
                            <img src="../../resources/img/delete.png" width="15" height="15" class="d-inline-block align-top" alt="">
                        </a>
                        <a href="/student/edit/${student.id}">
                            <img src="../../resources/img/edit.png" width="15" height="15" class="d-inline-block align-top" alt="">
                        </a>
                    </th>
                </sec:authorize>
            </tr>
        </c:forEach>
    </table>
</div>

<div class="container-fluid custom-container-centered">

    <c:if test="${studentCount >= 3}">
        <ul class="pagination">
            <c:choose>
                <c:when test="${currentPageNumber == 1}">
                    <li class="page-item disabled"><a class="page-link" href="/list/student/${currentPageNumber-1}">Previous</a></li>
                </c:when>
                <c:otherwise>
                    <li class="page-item"><a class="page-link" href="/list/student/${currentPageNumber-1}">Previous</a></li>
                </c:otherwise>
            </c:choose>

            <c:forEach var="numPage" begin="1" end="${studentCount / 3}">
                <li class="page-item"><a class="page-link" href="/list/student/${numPage}">${numPage}</a></li>
            </c:forEach>

            <c:if test="${studentCount % 3 != 0}">
                <li class="page-item"><a class="page-link" href="/list/student/${Math.floor(studentCount/3)+1}">${Math.floor(studentCount/3)+1}</a></li>
            </c:if>

            <c:choose>
                <c:when test="${studentCount / 3 <= currentPageNumber}">
                    <li class="page-item disabled"><a class="page-link" href="/list/student/${currentPageNumber+1}">Next</a></li>
                </c:when>
                <c:otherwise>
                    <li class="page-item"><a class="page-link" href="/list/student/${currentPageNumber+1}">Next</a></li>
                </c:otherwise>
            </c:choose>
        </ul>
    </c:if>

</body>
</html>
