<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <jsp:include page="../blocks/header.jsp" />
    <title>Personal page</title>
</head>

<jsp:include page="../blocks/navbar.jsp" />

<h1 class="jumbotron yellow">
    Welcome to your personal account
</h1>

<div class="container-fluid">

    <table class="table-bordered table table-hover">
        <tr>
            <td>
                Ваша почта:
            </td>
            <td>
                ${userViewModel.email}
            </td>
        </tr>
        <tr>
            <td>
                Ваша роль:
            </td>
            <td>
                <c:if test="${userViewModel.role.get(0) == 'ROLE_admin'}">
                    admin
                </c:if>

                <c:if test="${userViewModel.role.get(0) == 'ROLE_student'}">
                    student
                </c:if>
            </td>
        </tr>
    </table>
</div>

<body>


</body>
</html>
