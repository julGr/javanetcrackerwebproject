<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<html>
<head>
    <jsp:include page="../blocks/header.jsp" />
    <title>View all requests</title>
</head>
<body>

<jsp:include page="../blocks/navbar.jsp" />

<div class="container-fluid wrapper2">
    <h2>
        All requests
    </h2>
    <c:forEach items="${allRequests}" var="request">
        <div class="alert alert-warning">
            <H2>
                ${request.requestName}
            </H2>
            <p>
                <sec:authorize access="hasRole('ROLE_admin')">
                    <a href="/request/delete/${request.id}">
                        <img src="../../resources/img/delete.png" width="15" height="15" class="d-inline-block align-top" alt="">
                    </a>

                    <a href="/request/edit/${request.id}">
                        <img src="../../resources/img/edit.png" width="15" height="15" class="d-inline-block align-top" alt="">
                    </a>
                </sec:authorize>
            </p>
            <hr>
            <p>
                Period:
                ${request.dateFrom} - ${request.dateTo}
            </p>
            <p>
                Available speciality:
                ${request.availableSpecialtyName}
            </p>

            <sec:authorize access="hasRole('ROLE_admin')">
                <a type="submit" href="/request/manage/${request.id}" class="badge badge-danger show-manager">Manager</a>
            </sec:authorize>
        </div>
    </c:forEach>

<sec:authorize access="hasRole('ROLE_admin')">
    <a class="btn btn-outline-warning" href="/request/add">
        Add request
    </a>
</sec:authorize>

</div>

</body>
</html>
