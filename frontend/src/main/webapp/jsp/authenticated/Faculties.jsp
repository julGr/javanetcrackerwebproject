<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <jsp:include page="../blocks/header.jsp" />
    <title>View all faculties</title>
</head>
<body>

<jsp:include page="../blocks/navbar.jsp" />

<div class="container-fluid wrapper blocker">
    <h2>
        All faculties
    </h2>
    <div class="col-xs-12">

        <ul class="list-group">
            <c:forEach items="${allFaculties}" var="faculty">
                <li class="list-group-item list-group-item-warning">
                    <p>
                        ${faculty.facultyName}
                    </p>

                    <sec:authorize access="hasRole('ROLE_admin')">
                        <a href="/faculty/delete/${faculty.id}">
                            <img src="../../resources/img/delete.png" width="15" height="15" class="d-inline-block align-top" alt="">
                        </a>
                        <a href="/faculty/edit/${faculty.id}">
                            <img src="../../resources/img/edit.png" width="15" height="15" class="d-inline-block align-top" alt="">
                        </a>
                    </sec:authorize>
                </li>
            </c:forEach>
        </ul>
    </div>

    <sec:authorize access="hasRole('ROLE_admin')">
        <a class="btn btn-outline-warning" href="/faculty/add">
            Add faculty
        </a>
    </sec:authorize>

</div>

</body>
</html>