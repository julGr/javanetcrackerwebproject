<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <jsp:include page="../blocks/header.jsp" />
    <title>View all specialities</title>
</head>
<body>

<jsp:include page="../blocks/navbar.jsp" />

<div class="container-fluid">
    <h2>
        All specialities
    </h2>

    <c:forEach items="${allFaculties}"  var="faculty">

        <div class="alert-warning alert">
            <H2>
                ${faculty.facultyName}
            </H2>
            <hr>
            <c:forEach items="${allSpecialities}" var="speciality">
                <c:if test="${speciality.facultyName == faculty.facultyName}">
                    <p>
                        ${speciality.specialtyName}
                    </p>

                    <sec:authorize access="hasRole('ROLE_admin')">
                        <a href="/speciality/delete/${speciality.id}">
                            <img src="../../resources/img/delete.png" width="15" height="15" class="d-inline-block align-top" alt="">
                        </a>

                        <a href="/speciality/edit/${speciality.id}">
                            <img src="../../resources/img/edit.png" width="15" height="15" class="d-inline-block align-top" alt="">
                        </a>
                    </sec:authorize>
                </c:if>
            </c:forEach>
        </div>

    </c:forEach>

    <sec:authorize access="hasRole('ROLE_admin')">
        <a class="btn btn-outline-warning" href="/speciality/add">
            Add speciality
        </a>
    </sec:authorize>

</div>

</body>
</html>