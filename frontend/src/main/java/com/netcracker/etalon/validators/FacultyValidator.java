package com.netcracker.etalon.validators;

import com.netcracker.etalon.beans.FacultyViewModel;
import com.netcracker.etalon.servicies.FacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class FacultyValidator implements Validator {
    @Autowired
    private FacultyService facultyService;

    public boolean supports(Class<?> aClass) {
        return FacultyViewModel.class.isAssignableFrom(aClass);
    }

    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "facultyName", "faculty.name.empty");

        FacultyViewModel facultyViewModel = (FacultyViewModel) o;
        if (facultyService.findFaculty(facultyViewModel.getFacultyName())!= null)
            errors.rejectValue("facultyName", "faculty.name.invalid");
    }
}
