package com.netcracker.etalon.validators;

import com.netcracker.etalon.beans.RequestViewModel;
import com.netcracker.etalon.enteties.Request;
import com.netcracker.etalon.servicies.RequestService;
import com.netcracker.etalon.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class RequestValidator implements Validator{

    @Autowired
    private ConversionService conversionService;

    public boolean supports(Class<?> aClass) {
        return RequestViewModel.class.isAssignableFrom(aClass);
    }

    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "requestName", "request.name.empty");
        ValidationUtils.rejectIfEmpty(errors, "dateTo", "request.dateTo.empty");
        ValidationUtils.rejectIfEmpty(errors, "dateFrom", "request.dateFrom.empty");
        ValidationUtils.rejectIfEmpty(errors, "availableSpecialtyName", "request.availableSpecialtyName.empty");

        RequestViewModel requestViewModel = (RequestViewModel) o;

        if (!DateUtil.checkDateFormat(requestViewModel.getDateFrom(), "yyyy-MM-dd")){
            errors.rejectValue("dateFrom", "request.dateFrom.invalid");
        }
        if (!DateUtil.checkDateFormat(requestViewModel.getDateTo(), "yyyy-MM-dd")){
            errors.rejectValue("dateTo", "request.dateTo.invalid");
        }
        if(!DateUtil.checkRequestDates(conversionService.convert(requestViewModel, Request.class))){
            errors.rejectValue("dateTo", "request.date.invalid");
        }
    }
}
