package com.netcracker.etalon.validators;

import com.netcracker.etalon.beans.StudentViewModel;
import com.netcracker.etalon.servicies.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class StudentValidator implements Validator{

    public boolean supports(Class<?> aClass) {
        return StudentViewModel.class.isAssignableFrom(aClass);
    }

    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "fName", "student.fName.empty");
        ValidationUtils.rejectIfEmpty(errors, "lName", "student.lName.empty");
        ValidationUtils.rejectIfEmpty(errors, "specialtyName", "student.specialtyName.empty");
        ValidationUtils.rejectIfEmpty(errors, "stGroup", "student.stGroup.empty");
        ValidationUtils.rejectIfEmpty(errors, "isBudget", "student.isBudget.empty");
        ValidationUtils.rejectIfEmpty(errors, "averageScore", "student.averageScore.empty");

        StudentViewModel studentViewModel = (StudentViewModel) o;
        try {
            Double d = studentViewModel.getAverageScore();

            if (d>10 || d<1){
                errors.rejectValue("averageScore", "student.averageScore.invalid");
            }
        } catch (NumberFormatException e){
            errors.rejectValue("averageScore", "student.averageScore.invalid.type");
        }

        Pattern p = Pattern.compile("[0-9]{3,6}");

        Matcher m = p.matcher(studentViewModel.getStGroup());
        if (!m.matches())
            errors.rejectValue("stGroup", "student.stGroup.invalid");

        if(studentViewModel.getIsBudget().size() != 1)
            errors.rejectValue("isBudget", "student.isBudget.invalid");

    }
}
