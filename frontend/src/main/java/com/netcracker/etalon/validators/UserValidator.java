package com.netcracker.etalon.validators;

import com.netcracker.etalon.beans.UserViewModel;
import com.netcracker.etalon.servicies.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Pattern;

@Component
public class UserValidator implements Validator {
    @Autowired
    private ConversionService conversionService;

    @Autowired
    private UserService userService;

    public boolean supports(Class<?> aClass) {
        return UserViewModel.class.isAssignableFrom(aClass);
    }

    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "email", "user.email.empty");
        ValidationUtils.rejectIfEmpty(errors, "userPassword", "user.password.empty");
        ValidationUtils.rejectIfEmpty(errors, "role", "user.role.empty");

        UserViewModel userViewModel = (UserViewModel) o;

        Pattern pattern = Pattern.compile("^[\\w.-]+@\\w{3,10}\\.\\w{2,9}", Pattern.CASE_INSENSITIVE);
        if (!(pattern.matcher(userViewModel.getEmail()).matches()))
            errors.rejectValue("email", "user.email.invalid");

        if(userViewModel.getEmail().equals(userViewModel.getUserPassword()))
            errors.rejectValue("password", "user.password.invalid");

        if (conversionService.convert(userService.getUserByEmail(userViewModel.getEmail()), UserViewModel.class) != null)
            errors.rejectValue("email", "user.email.existing");

        if (userViewModel.getRole().size() !=1)
            errors.rejectValue("role", "user.role.invalid");
    }
}
