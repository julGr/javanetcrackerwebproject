package com.netcracker.etalon.validators;

import com.netcracker.etalon.beans.SpecialityViewModel;
import com.netcracker.etalon.enteties.Specialty;
import com.netcracker.etalon.servicies.SpecialtyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class SpecialityValidator  implements Validator{
    @Autowired
    private SpecialtyService specialtyService;

    public boolean supports(Class<?> aClass) {
        return SpecialityViewModel.class.isAssignableFrom(aClass);
    }

    public void validate(Object o, Errors errors) {

        ValidationUtils.rejectIfEmpty(errors, "specialtyName", "speciality.name.empty");
        ValidationUtils.rejectIfEmpty(errors, "facultyName", "speciality.facultyName.empty");

        SpecialityViewModel specialityViewModel = (SpecialityViewModel) o;

        Specialty specialtyWithTheSameName = specialtyService.findSpecialty(specialityViewModel.getSpecialtyName());
        if (specialtyWithTheSameName!= null){
            if(specialtyWithTheSameName.getFaculty().getFacultyName().equals(specialityViewModel.getFacultyName()))
                errors.rejectValue("specialtyName", "speciality.name.invalid");
        }

    }
}
