package com.netcracker.etalon.utils;

import com.netcracker.etalon.enteties.Request;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public abstract class DateUtil {

    static public boolean checkDateFormat(String value, String datePattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(datePattern);
        formatter.setLenient(false);
        try {
            formatter.parse(value);
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

    static public boolean checkRequestDates(Request request) {
        return request!=null && request.getDateFrom().before(request.getDateTo());
    }
}
