package com.netcracker.etalon.utils;

import com.netcracker.etalon.enteties.Request;
import com.netcracker.etalon.enteties.Student;

import java.util.ArrayList;
import java.util.List;

abstract public class RequestUtil {

    static public boolean checkRequestsConflict(Student student, Request request){
        for (Request studentsRequest: student.getRequests()){
            //начинается раньше, заканчивается позже
            if(studentsRequest.getDateFrom().before(request.getDateFrom()) &&
                    studentsRequest.getDateTo().after(request.getDateTo()))
                return true;
            //начинается раньше, заканчивается раньше
            if(studentsRequest.getDateFrom().before(request.getDateFrom()) &&
                    studentsRequest.getDateTo().after(request.getDateFrom()) &&
                    studentsRequest.getDateTo().before(request.getDateTo()))
                return true;
            //начинается позже, заканчивается позже
            if(studentsRequest.getDateFrom().after(request.getDateFrom()) &&
                    studentsRequest.getDateFrom().before(request.getDateTo())&&
                    studentsRequest.getDateTo().after(request.getDateTo()))
                return true;
            //начинается позже, заканчивается раньше
            if(studentsRequest.getDateFrom().after(request.getDateFrom()) &&
                    studentsRequest.getDateTo().before(request.getDateTo()))
                return true;
        }
        return false;
    }

    static public List<Student> returnValidStudentListForPractice(Request currentRequest, List<Student> allStudents){

        List<Student> validStudents =  new ArrayList<>();

        for (Student student: allStudents){
            if(!RequestUtil.checkRequestsConflict(student, currentRequest) &&
                    student.getSpecialty().getSpecialtyName().equals(currentRequest.getAvailableSpecialty().getSpecialtyName())){
                validStudents.add(student);
            }
        }

        return validStudents;
    }
}
