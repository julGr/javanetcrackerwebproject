package com.netcracker.etalon.utils;

import javax.servlet.http.HttpServletRequest;

abstract public class PagesUtil {
    static public String getPreviousPageUrl(HttpServletRequest httpServletRequest){
        String referer = httpServletRequest.getHeader("Referer");
        String localHostStr = "http://localhost:1111";
        String previousPageUrl = "#";

        if (referer!=null)
            previousPageUrl = referer.substring(localHostStr.length());

        return previousPageUrl;
    }
}
