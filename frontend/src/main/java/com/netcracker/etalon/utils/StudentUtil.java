package com.netcracker.etalon.utils;

import com.netcracker.etalon.enteties.Request;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

public abstract class StudentUtil {
    static public boolean checkIfStudentOnPractice(Request request){
        Date today = new Date();
        return !today.before(request.getDateFrom()) && !today.after(request.getDateTo());
    }

    static public String createStudentPracticePeriod(Set<Request> requests){
        String practicePeriod = "";
        String noPractice = " - ";
        String strFormat = "yyyy-MM-dd";

        SimpleDateFormat newFormat = new SimpleDateFormat(strFormat);

        if(!CollectionUtils.isEmpty(requests)){
            for (Request request: requests){
                if (checkIfStudentOnPractice(request)){
                    practicePeriod = newFormat.format(request.getDateFrom()) + " to " +
                            newFormat.format(request.getDateTo());
                    return practicePeriod;
                }
            }
        }
        practicePeriod = noPractice;
        return practicePeriod;
    }
}
