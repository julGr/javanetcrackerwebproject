package com.netcracker.etalon.beans;

import java.util.List;

public class StudentViewModel {
    private int id;
    private String fName;
    private String lName;
    private String specialtyName;
    private String facultyName;
    private String stGroup;
    private List<String> isBudget;
    private double averageScore;
    private List<String> status;
    private String practicePeriod;
    private List<String> requests;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getStGroup() {
        return stGroup;
    }

    public void setStGroup(String stGroup) {
        this.stGroup = stGroup;
    }

    public List<String> getIsBudget() {
        return isBudget;
    }

    public void setIsBudget(List<String> isBudget) {
        this.isBudget = isBudget;
    }

    public double getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(double averageScore) {
        this.averageScore = averageScore;
    }

    public List<String> getStatus() {
        return status;
    }

    public void setStatus(List<String> status) {
        this.status = status;
    }

    public String getPracticePeriod() {
        return practicePeriod;
    }

    public void setPracticePeriod(String practicePeriod) {
        this.practicePeriod = practicePeriod;
    }

    public String getSpecialtyName() {
        return specialtyName;
    }

    public void setSpecialtyName(String specialtyName) {
        this.specialtyName = specialtyName;
    }

    public List<String> getRequests() {
        return requests;
    }

    public void setRequests(List<String> requests) {
        this.requests = requests;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }
}
