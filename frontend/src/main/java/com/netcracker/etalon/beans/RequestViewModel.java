package com.netcracker.etalon.beans;

public class RequestViewModel {
    private int id;
    private String requestName;
    private String dateTo;
    private String dateFrom;
    private String availableSpecialtyName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRequestName() {
        return requestName;
    }

    public void setRequestName(String requestName) {
        this.requestName = requestName;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getAvailableSpecialtyName() {
        return availableSpecialtyName;
    }

    public void setAvailableSpecialtyName(String availableSpecialtyName) {
        this.availableSpecialtyName = availableSpecialtyName;
    }
}
