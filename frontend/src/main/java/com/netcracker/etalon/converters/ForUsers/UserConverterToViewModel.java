package com.netcracker.etalon.converters.ForUsers;

import com.netcracker.etalon.beans.UserViewModel;
import com.netcracker.etalon.enteties.Enums.RoleEnum;
import com.netcracker.etalon.enteties.User;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserConverterToViewModel implements Converter<User, UserViewModel> {

    @Override
    public UserViewModel convert(User user) {
        UserViewModel userViewModel = new UserViewModel();

        userViewModel.setId(user.getId());
        userViewModel.setEmail(user.getEmail());
        userViewModel.setUserPassword(user.getUserPassword());

        if (user.getRole().equals(RoleEnum.student))
            userViewModel.setRole(Arrays.asList("ROLE_student"));
        else
            userViewModel.setRole(Arrays.asList("ROLE_admin"));

        return userViewModel;
    }
}
