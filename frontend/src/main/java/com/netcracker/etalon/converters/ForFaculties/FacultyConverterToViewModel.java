package com.netcracker.etalon.converters.ForFaculties;

import com.netcracker.etalon.beans.FacultyViewModel;
import com.netcracker.etalon.enteties.Faculty;
import org.springframework.core.convert.converter.Converter;

public class FacultyConverterToViewModel implements Converter<Faculty, FacultyViewModel> {
    @Override
    public FacultyViewModel convert(Faculty faculty) {
        FacultyViewModel facultyViewModel = new FacultyViewModel();

        facultyViewModel.setId(faculty.getId());
        facultyViewModel.setFacultyName(faculty.getFacultyName());

        return facultyViewModel;
    }
}
