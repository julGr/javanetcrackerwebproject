package com.netcracker.etalon.converters.ForUsers;

import com.netcracker.etalon.beans.UserViewModel;
import com.netcracker.etalon.enteties.Enums.RoleEnum;
import com.netcracker.etalon.enteties.User;
import org.springframework.core.convert.converter.Converter;

import java.util.Arrays;

public class UserConverterToEntity implements Converter<UserViewModel, User> {

    @Override
    public User convert(UserViewModel userViewModel) {
        User user = new User();

        user.setId(userViewModel.getId());
        user.setEmail(userViewModel.getEmail());
        user.setUserPassword(userViewModel.getUserPassword());

        if (userViewModel.getRole().equals(Arrays.asList("ROLE_student"))) {
            user.setRole(RoleEnum.student);
        }
        else {
            user.setRole(RoleEnum.admin);
        }

        return user;
    }
}
