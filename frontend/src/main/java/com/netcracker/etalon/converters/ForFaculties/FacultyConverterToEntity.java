package com.netcracker.etalon.converters.ForFaculties;

import com.netcracker.etalon.beans.FacultyViewModel;
import com.netcracker.etalon.enteties.Faculty;
import org.springframework.core.convert.converter.Converter;

public class FacultyConverterToEntity implements Converter<FacultyViewModel, Faculty> {
    @Override
    public Faculty convert(FacultyViewModel facultyViewModel) {
        Faculty faculty = new Faculty();

        faculty.setId(facultyViewModel.getId());
        faculty.setFacultyName(facultyViewModel.getFacultyName());

        return faculty;
    }
}
