package com.netcracker.etalon.converters.ForSpecialities;

import com.netcracker.etalon.beans.SpecialityViewModel;
import com.netcracker.etalon.enteties.Specialty;
import com.netcracker.etalon.servicies.FacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

public class SpecialityConverterToEntity implements Converter<SpecialityViewModel, Specialty> {

    @Autowired
    private FacultyService facultyService;

    @Override
    public Specialty convert(SpecialityViewModel specialityViewModel) {
        Specialty specialty = new Specialty();

        specialty.setId(specialityViewModel.getId());
        specialty.setSpecialtyName(specialityViewModel.getSpecialtyName());
        specialty.setFaculty(facultyService.findFaculty(specialityViewModel.getFacultyName()));

        return specialty;
    }
}
