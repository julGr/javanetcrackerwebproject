package com.netcracker.etalon.converters.ForSpecialities;

import com.netcracker.etalon.beans.FacultyViewModel;
import com.netcracker.etalon.beans.SpecialityViewModel;
import com.netcracker.etalon.enteties.Faculty;
import com.netcracker.etalon.enteties.Specialty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;

public class SpecialityConverterToViewModel implements Converter<Specialty, SpecialityViewModel> {

    @Override
    public SpecialityViewModel convert(Specialty specialty) {
        SpecialityViewModel specialityViewModel = new SpecialityViewModel();

        specialityViewModel.setId(specialty.getId());
        specialityViewModel.setSpecialtyName(specialty.getSpecialtyName());
        specialityViewModel.setFacultyName(specialty.getFaculty().getFacultyName());

        return specialityViewModel;
    }
}
