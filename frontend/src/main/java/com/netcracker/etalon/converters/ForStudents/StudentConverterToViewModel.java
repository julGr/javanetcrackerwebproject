package com.netcracker.etalon.converters.ForStudents;

import com.netcracker.etalon.beans.RequestViewModel;
import com.netcracker.etalon.beans.StudentViewModel;
import com.netcracker.etalon.enteties.Enums.IsBudgetEnum;
import com.netcracker.etalon.enteties.Request;
import com.netcracker.etalon.enteties.Student;
import com.netcracker.etalon.utils.StudentUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StudentConverterToViewModel implements Converter<Student, StudentViewModel> {
    @Autowired
    private ConversionService conversionService;

    @Override
    public StudentViewModel convert(Student student) {
        StudentViewModel studentViewModel = new StudentViewModel();

        List<String> requestsNames = new ArrayList<>();

        studentViewModel.setId(student.getId());
        studentViewModel.setfName(student.getfName());
        studentViewModel.setlName(student.getlName());
        studentViewModel.setAverageScore(student.getAverageScore());
        studentViewModel.setStGroup(student.getStGroup());
        studentViewModel.setSpecialtyName(student.getSpecialty().getSpecialtyName());
        studentViewModel.setFacultyName(student.getSpecialty().getFaculty().getFacultyName());

        if(student.getIsBudget().equals(IsBudgetEnum.yes))
            studentViewModel.setIsBudget(Arrays.asList("yes"));
        else
            studentViewModel.setIsBudget(Arrays.asList("no"));

        if (!student.getRequests().isEmpty()){
            for (Request request: student.getRequests()) {
                if (StudentUtil.checkIfStudentOnPractice(request))
                    studentViewModel.setStatus(Arrays.asList("on practice"));
                else
                    studentViewModel.setStatus(Arrays.asList("free"));

                requestsNames.add(conversionService.convert(request, RequestViewModel.class).getRequestName());
            }
            studentViewModel.setRequests(requestsNames);
            studentViewModel.setPracticePeriod(StudentUtil.createStudentPracticePeriod(student.getRequests()));
        }
        else {
            studentViewModel.setStatus(Arrays.asList("free"));
            studentViewModel.setPracticePeriod(" - ");
        }

        studentViewModel.setPracticePeriod(StudentUtil.createStudentPracticePeriod(student.getRequests()));

        return studentViewModel;
    }
}
