package com.netcracker.etalon.converters.ForRequests;

import com.netcracker.etalon.beans.RequestViewModel;
import com.netcracker.etalon.enteties.Request;
import org.springframework.core.convert.converter.Converter;

import java.text.SimpleDateFormat;

public class RequestConverterToViewModel implements Converter<Request, RequestViewModel> {
    @Override
    public RequestViewModel convert(Request request) {
        RequestViewModel requestViewModel = new RequestViewModel();

        SimpleDateFormat newFormatter = new SimpleDateFormat("yyyy-MM-dd");

        requestViewModel.setId(request.getId());
        requestViewModel.setRequestName(request.getRequestName());
        requestViewModel.setDateFrom(newFormatter.format(request.getDateFrom()));
        requestViewModel.setDateTo(newFormatter.format(request.getDateTo()));
        requestViewModel.setAvailableSpecialtyName(request.getAvailableSpecialty().getSpecialtyName());

        return requestViewModel;
    }
}
