package com.netcracker.etalon.converters.ForStudents;

import com.netcracker.etalon.beans.StudentViewModel;
import com.netcracker.etalon.enteties.Enums.IsBudgetEnum;
import com.netcracker.etalon.enteties.Enums.StatusEnum;
import com.netcracker.etalon.enteties.Request;
import com.netcracker.etalon.enteties.Student;
import com.netcracker.etalon.servicies.RequestService;
import com.netcracker.etalon.servicies.SpecialtyService;
import com.netcracker.etalon.utils.StudentUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import java.util.*;

public class StudentConverterToEntity implements Converter<StudentViewModel, Student> {

    @Autowired
    private SpecialtyService specialtyService;

    @Autowired
    private RequestService requestService;

    @Override
    public Student convert(StudentViewModel studentViewModel) {
        Student student = new Student();

        Set<Request> requests = new HashSet<>();

        student.setId(studentViewModel.getId());
        student.setfName(studentViewModel.getfName());
        student.setlName(studentViewModel.getlName());
        student.setAverageScore(studentViewModel.getAverageScore());
        student.setStGroup(studentViewModel.getStGroup());

        if(studentViewModel.getIsBudget().equals(Arrays.asList("yes")))
            student.setIsBudget(IsBudgetEnum.yes);
        else
            student.setIsBudget(IsBudgetEnum.no);

        student.setSpecialty(specialtyService.findSpecialty(studentViewModel.getSpecialtyName()));
        student.setFaculty(specialtyService.findSpecialty(studentViewModel.getSpecialtyName()).getFaculty());

        if (studentViewModel.getRequests() != null){
            for(String requestViewModelName: studentViewModel.getRequests()) {
                Request request = requestService.findRequest(requestViewModelName);

                if(StudentUtil.checkIfStudentOnPractice(request))
                    student.setStatus(StatusEnum.on_practice);
                else student.setStatus(StatusEnum.free);

                requests.add(request);
            }

            student.setRequests(requests);

            studentViewModel.setPracticePeriod(StudentUtil.createStudentPracticePeriod(requests));
        } else {
            student.setPracticePeriod(" - ");
            student.setStatus(StatusEnum.free);
        }

        return student;
    }
}
