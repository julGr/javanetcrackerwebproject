package com.netcracker.etalon.converters.ForRequests;

import com.netcracker.etalon.beans.RequestViewModel;
import com.netcracker.etalon.enteties.Request;
import com.netcracker.etalon.servicies.FacultyService;
import com.netcracker.etalon.servicies.SpecialtyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class RequestConverterToEntity implements Converter<RequestViewModel, Request> {

    @Autowired
    private SpecialtyService specialtyService;

    @Override
    public Request convert(RequestViewModel requestViewModel) {
        Request request = new Request();

        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");

        request.setId(requestViewModel.getId());
        request.setRequestName(requestViewModel.getRequestName());

        try {
            request.setDateFrom(newFormat.parse(requestViewModel.getDateFrom()));
            request.setDateTo(newFormat.parse(requestViewModel.getDateTo()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        request.setAvailableSpecialty(specialtyService.findSpecialty(requestViewModel.getAvailableSpecialtyName()));
        request.setAvailableFaculty(specialtyService.findSpecialty(requestViewModel.getAvailableSpecialtyName()).getFaculty());

        return request;
    }
}
