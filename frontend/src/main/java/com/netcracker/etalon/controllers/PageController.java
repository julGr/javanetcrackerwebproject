package com.netcracker.etalon.controllers;

import com.netcracker.etalon.beans.UserViewModel;
import com.netcracker.etalon.utils.PagesUtil;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

@Controller
@SessionAttributes("userViewModel")
public class PageController {

    private static final String MODEL_NAME_USER = "userViewModel";
    private static final String MODEL_NAME_PREVIOUS_PAGE = "previousPage";

    @ModelAttribute("userViewModel")
    public UserViewModel createUserViewModel(){
        return new UserViewModel();
    }

    @RequestMapping(value = "authorization", method = RequestMethod.GET)
    public ModelAndView getAuthorization(){
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userRole = null;

        for (GrantedAuthority authority : user.getAuthorities()) {
            userRole = authority.getAuthority();
        }

        UserViewModel userViewModel = new UserViewModel();
        userViewModel.setEmail(user.getUsername());
        userViewModel.setRole(Arrays.asList(userRole));

        return new ModelAndView("authenticated/PersonalArea", MODEL_NAME_USER, userViewModel);
    }

    @RequestMapping("error")
    public ModelAndView getError(HttpServletRequest request){
        int status_code = (Integer) request.getAttribute("javax.servlet.error.status_code");
        String viewName = "";
        String previousPage = PagesUtil.getPreviousPageUrl(request);
        if(previousPage.equals("#"))
            previousPage = "/profile";

        switch (status_code){
            case 404:
                viewName = "errors/NotFound";
                break;
            case 403:
                viewName = "errors/AccessDenied";
                previousPage = "/profile";
                break;
            default: viewName = "errors/ErrorPage";
                previousPage = "/profile";
                break;
        }
        return new ModelAndView(viewName, MODEL_NAME_PREVIOUS_PAGE, previousPage);
    }

}