package com.netcracker.etalon.controllers;

import com.netcracker.etalon.beans.FacultyViewModel;
import com.netcracker.etalon.beans.RequestViewModel;
import com.netcracker.etalon.beans.SpecialityViewModel;
import com.netcracker.etalon.beans.StudentViewModel;
import com.netcracker.etalon.enteties.Faculty;
import com.netcracker.etalon.enteties.Request;
import com.netcracker.etalon.enteties.Specialty;
import com.netcracker.etalon.enteties.Student;
import com.netcracker.etalon.servicies.FacultyService;
import com.netcracker.etalon.servicies.RequestService;
import com.netcracker.etalon.servicies.SpecialtyService;
import com.netcracker.etalon.servicies.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("list")
public class ViewListController {
    @Autowired
    public FacultyService facultyService;

    @Autowired
    public SpecialtyService specialtyService;

    @Autowired
    public RequestService requestService;

    @Autowired
    public ConversionService conversionService;

    @Autowired
    private StudentService studentService;

    private static final String MODEL_NAME_ALL_STUDENTS = "allStudents";
    private static final String MODEL_NAME_ALL_SPECIALITIES = "allSpecialities";
    private static final String MODEL_NAME_ALL_REQUESTS = "allRequests";
    private static final String MODEL_NAME_ALL_FACULTIES = "allFaculties";
    private static final String  MODEL_NAME_STUDENT_COUNT = "studentCount";
    private static final String MODEL_NAME_CURRENT_PAGE_NUMBER = "currentPageNumber";
    private static final String MODEL_NAME_PREVIOUS_PAGE = "previousPage";

    private final TypeDescriptor studentsTypeDecriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(Student.class));
    private final TypeDescriptor studentsViewModelTypeDecriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(StudentViewModel.class));

    private final TypeDescriptor specialityTypeDecriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(Specialty.class));
    private final TypeDescriptor specialityViewModelTypeDecriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(SpecialityViewModel.class));

    private final TypeDescriptor facultyTypeDecriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(Faculty.class));
    private final TypeDescriptor facultyViewModelTypeDecriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(FacultyViewModel.class));

    private final TypeDescriptor requestTypeDecriptor= TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(Request.class));
    private final TypeDescriptor requestViewModelTypeDecriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(RequestViewModel.class));

    @RequestMapping("faculty")
    public ModelAndView getAllFaculties(){
        return new ModelAndView("authenticated/Faculties", MODEL_NAME_ALL_FACULTIES, conversionService.convert(
                facultyService.allFaculties(), facultyTypeDecriptor, facultyViewModelTypeDecriptor));
    }

    @RequestMapping("speciality")
    public ModelAndView getAllSpecialities(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("authenticated/Specialities");

        modelAndView.addObject(MODEL_NAME_ALL_SPECIALITIES,
                conversionService.convert(specialtyService.allSpecialties(), specialityTypeDecriptor, specialityViewModelTypeDecriptor));
        modelAndView.addObject(MODEL_NAME_ALL_FACULTIES,
                conversionService.convert(facultyService.allFaculties(), facultyTypeDecriptor, facultyViewModelTypeDecriptor));
        return modelAndView;
    }

    @RequestMapping("request")
    public  ModelAndView getAllRequests(){
        return new ModelAndView("authenticated/Requests", MODEL_NAME_ALL_REQUESTS, conversionService.convert(
                requestService.allRequests(), requestTypeDecriptor, requestViewModelTypeDecriptor));
    }

    @RequestMapping(value = "student/{pageNumber}", method = RequestMethod.GET)
    public ModelAndView getAllStudentsPages(@PathVariable int pageNumber, HttpServletRequest request){

        int studentsCount = studentService.allStudents().size();

        if ((studentsCount %3 != 0 &&
                pageNumber > studentsCount /3 + 1) ||
                (studentsCount % 3 == 0 &&
                        pageNumber > studentsCount / 3) ||
                pageNumber < 1)
            return new ModelAndView("errors/NotFound", MODEL_NAME_PREVIOUS_PAGE, "/list/student/1");

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("authenticated/InfoTable");
        modelAndView.addObject(MODEL_NAME_ALL_SPECIALITIES, conversionService.convert(
                specialtyService.allSpecialties(), specialityTypeDecriptor, specialityViewModelTypeDecriptor));
        modelAndView.addObject(MODEL_NAME_ALL_FACULTIES, conversionService.convert(
                facultyService.allFaculties(), facultyTypeDecriptor, facultyViewModelTypeDecriptor));
        modelAndView.addObject(MODEL_NAME_ALL_REQUESTS, conversionService.convert(
                requestService.allRequests(), requestTypeDecriptor, requestViewModelTypeDecriptor));

        if(studentsCount / 3 <= pageNumber &&
                studentsCount / 3 >= pageNumber - 1) {
            modelAndView.addObject(MODEL_NAME_ALL_STUDENTS, conversionService.convert(
                    studentService.findAllLimit(3, 3 * (pageNumber - 1)), studentsTypeDecriptor, studentsViewModelTypeDecriptor));
        } else if(pageNumber == 1){
            modelAndView.addObject(MODEL_NAME_ALL_STUDENTS, conversionService.convert(
                    studentService.findAllLimit(3, 0), studentsTypeDecriptor, studentsViewModelTypeDecriptor));
        }

        modelAndView.addObject(MODEL_NAME_STUDENT_COUNT, studentsCount);
        modelAndView.addObject(MODEL_NAME_CURRENT_PAGE_NUMBER, pageNumber);

        return modelAndView;
    }

    @RequestMapping("filterByFaculty/{facultyId}")
    public ModelAndView getAllStudentsInFaculty(@PathVariable int facultyId){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("authenticated/InfoTable");
        modelAndView.addObject(MODEL_NAME_ALL_STUDENTS, conversionService.convert(
                studentService.allStudentsOnFaculty(facultyService.findById(facultyId)), studentsTypeDecriptor, studentsViewModelTypeDecriptor) );
        modelAndView.addObject(MODEL_NAME_ALL_SPECIALITIES, conversionService.convert(
                specialtyService.allSpecialties(), specialityTypeDecriptor, specialityViewModelTypeDecriptor));
        modelAndView.addObject(MODEL_NAME_ALL_FACULTIES, conversionService.convert(
                facultyService.allFaculties(), facultyTypeDecriptor, facultyViewModelTypeDecriptor));
        modelAndView.addObject(MODEL_NAME_ALL_REQUESTS, conversionService.convert(
                requestService.allRequests(), requestTypeDecriptor, requestViewModelTypeDecriptor));
        return modelAndView;
    }

    @RequestMapping("filterBySpeciality/{specialityId}")
    public ModelAndView getAllStudentsInSpeciality(@PathVariable int specialityId){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("authenticated/InfoTable");
        modelAndView.addObject(MODEL_NAME_ALL_STUDENTS, conversionService.convert(
                studentService.allStudentsOnSpecialty(specialtyService.findById(specialityId)), studentsTypeDecriptor, studentsViewModelTypeDecriptor) );
        modelAndView.addObject(MODEL_NAME_ALL_SPECIALITIES, conversionService.convert(
                specialtyService.allSpecialties(), specialityTypeDecriptor, specialityViewModelTypeDecriptor));
        modelAndView.addObject(MODEL_NAME_ALL_FACULTIES, conversionService.convert(
                facultyService.allFaculties(), facultyTypeDecriptor, facultyViewModelTypeDecriptor));
        modelAndView.addObject(MODEL_NAME_ALL_REQUESTS, conversionService.convert(
                requestService.allRequests(), requestTypeDecriptor, requestViewModelTypeDecriptor));
        return modelAndView;
    }

}
