package com.netcracker.etalon.controllers;

import com.netcracker.etalon.beans.FacultyViewModel;
import com.netcracker.etalon.validators.StudentValidator;
import com.netcracker.etalon.beans.RequestViewModel;
import com.netcracker.etalon.beans.SpecialityViewModel;
import com.netcracker.etalon.beans.StudentViewModel;
import com.netcracker.etalon.enteties.Request;
import com.netcracker.etalon.enteties.Specialty;
import com.netcracker.etalon.enteties.Student;
import com.netcracker.etalon.servicies.RequestService;
import com.netcracker.etalon.servicies.SpecialtyService;
import com.netcracker.etalon.servicies.StudentService;
import com.netcracker.etalon.utils.PagesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("student")
public class StudentFormController {

    @Autowired
    private ConversionService conversionService;

    @Autowired
    private RequestService requestService;

    @Autowired
    private SpecialtyService specialtyService;

    @Autowired
    private StudentValidator studentValidator;

    @Autowired
    private StudentService studentService;

    private static final String MODEL_NAME_PREVIOUS_PAGE = "previousPage";
    private static final String MODEL_NAME_ALL_SPECIALITIES = "allSpeciality";
    private static final String MODEL_NAME_ALL_REQUESTS = "allRequests";
    private static final String MODEL_NAME_STUDENT = "studentViewModel";
    private static final String VIEW_NAME_FORM = "authenticated/admin/FormForStudent";
    private static final String MODEL_NAME_EDITED_ID = "edited_student_id";

    private final TypeDescriptor specialityTypeDecriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(Specialty.class));
    private final TypeDescriptor specialityViewModelTypeDecriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(SpecialityViewModel.class));

    private final TypeDescriptor requestTypeDecriptor= TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(Request.class));
    private final TypeDescriptor requestViewModelTypeDecriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(RequestViewModel.class));

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public ModelAndView getRegistrationForm(HttpServletRequest httpServletRequest){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(VIEW_NAME_FORM);
        modelAndView.addObject(MODEL_NAME_STUDENT, new StudentViewModel());

        modelAndView.addObject(MODEL_NAME_ALL_SPECIALITIES, conversionService.convert(
                specialtyService.allSpecialties(), specialityTypeDecriptor, specialityViewModelTypeDecriptor));

        modelAndView.addObject(MODEL_NAME_ALL_REQUESTS, conversionService.convert(
                requestService.allRequests(), requestTypeDecriptor, requestViewModelTypeDecriptor));
        modelAndView.addObject(MODEL_NAME_PREVIOUS_PAGE, PagesUtil.getPreviousPageUrl(httpServletRequest));
        return modelAndView;
    }

    @RequestMapping(value = "submit", method = RequestMethod.POST)
    public ModelAndView submitRegistrationForm(@ModelAttribute(MODEL_NAME_STUDENT) StudentViewModel studentViewModel,
                                               BindingResult bindingResult){
        studentValidator.validate(studentViewModel, bindingResult);

        if (bindingResult.hasErrors()){
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName(VIEW_NAME_FORM);
            modelAndView.addObject(MODEL_NAME_STUDENT, studentViewModel);

            modelAndView.addObject(MODEL_NAME_ALL_SPECIALITIES, conversionService.convert(
                    specialtyService.allSpecialties(), specialityTypeDecriptor, specialityViewModelTypeDecriptor));

            modelAndView.addObject(MODEL_NAME_ALL_REQUESTS, conversionService.convert(
                    requestService.allRequests(),
                    requestTypeDecriptor, requestViewModelTypeDecriptor));

            return modelAndView;
        }
        else
        {
            studentService.addStudent(conversionService.convert(studentViewModel, Student.class));
            return new ModelAndView("redirect:/list/student/1");
        }

    }

    @RequestMapping(value = "edit/{studentId}", method = RequestMethod.GET)
    public ModelAndView editFaculty(@PathVariable int studentId, HttpServletRequest httpServletRequest){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(VIEW_NAME_FORM);
        modelAndView.addObject(conversionService.convert(
                studentService.findById(studentId), StudentViewModel.class));

        modelAndView.addObject(MODEL_NAME_ALL_SPECIALITIES, conversionService.convert(
                specialtyService.allSpecialties(), specialityTypeDecriptor, specialityViewModelTypeDecriptor));

        modelAndView.addObject(MODEL_NAME_ALL_REQUESTS, conversionService.convert(
                requestService.allRequests(), requestTypeDecriptor, requestViewModelTypeDecriptor));
        modelAndView.addObject(MODEL_NAME_PREVIOUS_PAGE, PagesUtil.getPreviousPageUrl(httpServletRequest));
        modelAndView.addObject(MODEL_NAME_EDITED_ID, studentId);
        return modelAndView;
    }

    @RequestMapping(value = "submit/{studentId}", method = RequestMethod.POST)
    public String submitEdit(@PathVariable int studentId, @ModelAttribute(MODEL_NAME_STUDENT) StudentViewModel studentViewModel,
                             BindingResult bindingResult){
        studentValidator.validate(studentViewModel, bindingResult);
        if (bindingResult.hasErrors())
            return VIEW_NAME_FORM;
        else
        {
            Student editedStudent = conversionService.convert(studentViewModel, Student.class);
            studentService.update(editedStudent.getfName(), editedStudent.getlName(), editedStudent.getSpecialty(),
                    editedStudent.getStGroup(), editedStudent.getIsBudget(), editedStudent.getAverageScore(),
                    editedStudent.getId());
            return "redirect:/list/student/1";
        }
    }

    @RequestMapping(value = "delete/{studentId}", method = RequestMethod.GET)
    public String deleteStudent(@PathVariable int studentId, Model model, HttpServletRequest request){
        try {
            studentService.deleteStudent(studentService.findById(studentId));
        } catch (DataIntegrityViolationException e){
            model.addAttribute(MODEL_NAME_PREVIOUS_PAGE, PagesUtil.getPreviousPageUrl(request));
            return "errors/CanNotRemove";
        }
        return "redirect:/list/student/1";
    }
}
