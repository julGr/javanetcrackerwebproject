package com.netcracker.etalon.controllers;

import com.netcracker.etalon.beans.FacultyViewModel;
import com.netcracker.etalon.enteties.Faculty;
import com.netcracker.etalon.servicies.FacultyService;
import com.netcracker.etalon.utils.PagesUtil;
import com.netcracker.etalon.validators.FacultyValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("faculty")
public class FacultyFormController {

    @Autowired
    private FacultyService facultyService;

    @Autowired
    private ConversionService conversionService;

    @Autowired
    private FacultyValidator facultyValidator;

    private static final String MODEL_NAME_FACULTY = "facultyViewModel";
    private static final String VIEW_NAME_FORM = "authenticated/admin/FormForFaculty";
    private static final String MODEL_NAME_EDITED_FACULTY_ID = "edited_faculty_id";
    private static final String  MODEL_NAME_PREVIOUS_PAGE = "previousPage";

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public ModelAndView getFormForFaculty(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(VIEW_NAME_FORM);
        modelAndView.addObject(MODEL_NAME_FACULTY, new FacultyViewModel());
        modelAndView.addObject("previousPage", PagesUtil.getPreviousPageUrl(request));
        return modelAndView;
    }

    @RequestMapping(value = "submit", method = RequestMethod.POST)
    public String submitRegistrationForm(@ModelAttribute(MODEL_NAME_FACULTY) FacultyViewModel facultyViewModel,
                                         BindingResult bindingResult){
        facultyValidator.validate(facultyViewModel, bindingResult);

        if (bindingResult.hasErrors()){
            return VIEW_NAME_FORM;
        }
        else
        {
            facultyService.addFaculty(conversionService.convert(facultyViewModel, Faculty.class));
            return "redirect:/list/faculty";
        }
    }

    @RequestMapping(value = "delete/{facultyId}", method = RequestMethod.GET)
    public String deleteFaculty(@PathVariable int facultyId, Model model, HttpServletRequest request){
        try {
            facultyService.deleteFaculty(facultyService.findById(facultyId));

        } catch (DataIntegrityViolationException e){
            model.addAttribute("previousPage", PagesUtil.getPreviousPageUrl(request));
            return "errors/CanNotRemove";
        }
        return "redirect:/list/faculty";
    }

    @RequestMapping(value = "edit/{facultyId}", method = RequestMethod.GET)
    public ModelAndView editFaculty(@PathVariable int facultyId, HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(VIEW_NAME_FORM);
        modelAndView.addObject(conversionService.convert(
                facultyService.findById(facultyId), FacultyViewModel.class));
        modelAndView.addObject(MODEL_NAME_EDITED_FACULTY_ID, facultyId);
        modelAndView.addObject(MODEL_NAME_PREVIOUS_PAGE, PagesUtil.getPreviousPageUrl(request));
        return modelAndView;
    }

    @RequestMapping(value = "submit/{facultyId}", method = RequestMethod.POST)
    public String submitEdit(@PathVariable int facultyId, @ModelAttribute(MODEL_NAME_FACULTY) FacultyViewModel facultyViewModel,
                             BindingResult bindingResult){
        facultyValidator.validate(facultyViewModel, bindingResult);
        if (bindingResult.hasErrors())
            return VIEW_NAME_FORM;
        else
        {
            facultyService.updateFaculty(facultyViewModel.getFacultyName(), facultyId);
            return "redirect:/list/faculty";
        }
    }
}
