package com.netcracker.etalon.controllers;

import com.netcracker.etalon.validators.RequestValidator;
import com.netcracker.etalon.beans.RequestViewModel;
import com.netcracker.etalon.beans.SpecialityViewModel;
import com.netcracker.etalon.beans.StudentViewModel;
import com.netcracker.etalon.enteties.Request;
import com.netcracker.etalon.enteties.Specialty;
import com.netcracker.etalon.enteties.Student;
import com.netcracker.etalon.servicies.RequestService;
import com.netcracker.etalon.servicies.SpecialtyService;
import com.netcracker.etalon.servicies.StudentService;
import com.netcracker.etalon.utils.PagesUtil;
import com.netcracker.etalon.utils.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
@RequestMapping("request")
public class RequestFormController {

    @Autowired
    private RequestService requestService;

    @Autowired
    private ConversionService conversionService;

    @Autowired
    private SpecialtyService specialtyService;

    @Autowired
    private RequestValidator requestValidator;

    @Autowired
    private StudentService studentService;

    private static final String MODEL_NAME_REQUEST = "requestViewModel";
    private static final String MODEL_NAME_ALL_SPECIALITIES = "allSpeciality";
    private static final String MODEL_NAME_ALL_STUDENTS = "allStudents";
    private static final String VIEW_NAME_FORM = "authenticated/admin/FormForRequest";
    private static final String VIEW_NAME_REQUEST_MANAGER = "authenticated/admin/RequestManageStudent";
    private static final String MODEL_NAME_PREVIOUS_PAGE = "previousPage";
    private static final String MODEL_NAME_EDITED_ID = "edited_request_id";

    private final TypeDescriptor specialityTypeDecriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(Specialty.class));
    private final TypeDescriptor specialityViewModelTypeDecriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(SpecialityViewModel.class));

    private final TypeDescriptor studentsTypeDecriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(Student.class));
    private final TypeDescriptor studentsViewModelTypeDecriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(StudentViewModel.class));

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public ModelAndView getRegistrationForm(HttpServletRequest httpServletRequest){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(VIEW_NAME_FORM);
        modelAndView.addObject(MODEL_NAME_REQUEST, new RequestViewModel());
        modelAndView.addObject(MODEL_NAME_ALL_SPECIALITIES, conversionService.convert(
                specialtyService.allSpecialties(), specialityTypeDecriptor, specialityViewModelTypeDecriptor));
        modelAndView.addObject(MODEL_NAME_PREVIOUS_PAGE, PagesUtil.getPreviousPageUrl(httpServletRequest));
        return modelAndView;
    }

    @RequestMapping(value = "submit", method = RequestMethod.POST)
    public ModelAndView submitRegistrationForm(@ModelAttribute(MODEL_NAME_REQUEST) RequestViewModel requestViewModel,
                                               BindingResult bindingResult){
        requestValidator.validate(requestViewModel, bindingResult);
        if (bindingResult.hasErrors()){
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName(VIEW_NAME_FORM);
            modelAndView.addObject(MODEL_NAME_REQUEST, requestViewModel);
            modelAndView.addObject(MODEL_NAME_ALL_SPECIALITIES, conversionService.convert(
                    specialtyService.allSpecialties(), specialityTypeDecriptor, specialityViewModelTypeDecriptor));
            return modelAndView;
        }
        else
        {
            requestService.addRequest(conversionService.convert(requestViewModel, Request.class));
            return new ModelAndView("redirect:/list/request");
        }
    }

    @RequestMapping(value = "manage/{requestId}", method = RequestMethod.GET)
    public ModelAndView getRequestManager(@PathVariable int requestId, HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView();

        Request currentRequest = requestService.findById(requestId);

        modelAndView.setViewName(VIEW_NAME_REQUEST_MANAGER);
        modelAndView.addObject(MODEL_NAME_REQUEST, conversionService.convert(
                currentRequest, RequestViewModel.class));
        modelAndView.addObject(MODEL_NAME_ALL_STUDENTS,
                conversionService.convert(RequestUtil.returnValidStudentListForPractice(
                        currentRequest, studentService.allStudents()), studentsTypeDecriptor,
                        studentsViewModelTypeDecriptor));
        modelAndView.addObject(MODEL_NAME_PREVIOUS_PAGE, "/list/request");

        return modelAndView;
    }

    @RequestMapping(value = "manager/{requestId}/{studentId}", method = RequestMethod.GET)
    public String manageStudentForRequest(@PathVariable int requestId, @PathVariable int studentId){

        Student student = studentService.findById(studentId);
        Set<Request> requests = student.getRequests();

        requests.add(requestService.findById(requestId));

        student.setRequests(requests);

        studentService.deleteStudent(studentService.findById(studentId));
        studentService.addStudent(student);

        return String.format("redirect:/request/manage/%d", requestId);
    }

    @RequestMapping(value = "manager/cancel/{requestId}/{studentId}", method = RequestMethod.GET)
    public String cancelStudentForRequest(@PathVariable int requestId, @PathVariable int studentId){

        Student student = studentService.findById(studentId);
        String requestNameToRemove = requestService.findById(requestId).getRequestName();
        Request foundRequest = requestService.findById(requestId);

        Set<Request> requests = student.getRequests();
        Set<Request> newRequests = new HashSet<>();

        for(Request request: requests){
            if (!request.getRequestName().equals(requestNameToRemove))
                newRequests.add(request);
        }

        student.setRequests(newRequests);
        studentService.deleteStudent(studentService.findById(studentId));
        studentService.addStudent(student);
        requestService.addRequest(foundRequest);

        return String.format("redirect:/request/manage/%d", requestId);
    }

    @RequestMapping(value = "delete/{requestId}", method = RequestMethod.GET)
    public String deleteRequest(@PathVariable int requestId, Model model, HttpServletRequest request){
        requestService.deleteRequest(requestService.findById(requestId));
        model.addAttribute(MODEL_NAME_PREVIOUS_PAGE, PagesUtil.getPreviousPageUrl(request));
        return "redirect:/list/request";
    }

    @RequestMapping(value = "edit/{requestId}", method = RequestMethod.GET)
    public ModelAndView editFaculty(@PathVariable int requestId, HttpServletRequest httpServletRequest){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(VIEW_NAME_FORM);
        modelAndView.addObject(conversionService.convert(requestService.findById(requestId),
                RequestViewModel.class));
        modelAndView.addObject(MODEL_NAME_ALL_SPECIALITIES, conversionService.convert(
                specialtyService.allSpecialties(), specialityTypeDecriptor, specialityViewModelTypeDecriptor));
        modelAndView.addObject(MODEL_NAME_EDITED_ID, requestId);
        modelAndView.addObject(MODEL_NAME_PREVIOUS_PAGE, PagesUtil.getPreviousPageUrl(httpServletRequest));
        return modelAndView;
    }

    @RequestMapping(value = "submit/{requestId}", method = RequestMethod.POST)
    public String submitEdit(@PathVariable int requestId, @ModelAttribute(MODEL_NAME_REQUEST) RequestViewModel requestViewModel,
                             BindingResult bindingResult){
        requestValidator.validate(requestViewModel, bindingResult);
        if (bindingResult.hasErrors())
            return VIEW_NAME_FORM;
        else
        {
            Request editedRequest = conversionService.convert(requestViewModel, Request.class);
            requestService.update(editedRequest.getRequestName(), editedRequest.getDateFrom(), editedRequest.getDateTo(),
            editedRequest.getAvailableFaculty(), editedRequest.getAvailableSpecialty(), requestId);

            return "redirect:/list/request";
        }
    }
}