package com.netcracker.etalon.controllers;

import com.netcracker.etalon.validators.SpecialityValidator;
import com.netcracker.etalon.beans.FacultyViewModel;
import com.netcracker.etalon.beans.SpecialityViewModel;
import com.netcracker.etalon.enteties.Faculty;
import com.netcracker.etalon.enteties.Specialty;
import com.netcracker.etalon.servicies.FacultyService;
import com.netcracker.etalon.servicies.SpecialtyService;
import com.netcracker.etalon.utils.PagesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("speciality")
public class SpecialityFormController {

    @Autowired
    private SpecialtyService specialtyService;

    @Autowired
    private ConversionService conversionService;

    @Autowired
    private FacultyService facultyService;

    @Autowired
    private SpecialityValidator specialityValidator;

    private static final String MODEL_NAME_SPECIALITY = "specialityViewModel";
    private static final String MODEL_NAME_ALL_FACULTIES = "allFaculties";
    private static final String VIEW_NAME_FORM = "authenticated/admin/FormForSpeciality";
    private static final String MODEL_NAME_EDITED_ID = "edited_speciality_id";
    private static final String MODEL_NAME_PREVIOUS_PAGE = "previousPage";

    private final TypeDescriptor facultyTypeDecriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(Faculty.class));
    private final TypeDescriptor facultyViewModelTypeDecriptor = TypeDescriptor.collection(List.class, TypeDescriptor.valueOf(FacultyViewModel.class));

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public ModelAndView getRegistrationForm(HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(VIEW_NAME_FORM);
        modelAndView.addObject(MODEL_NAME_SPECIALITY, new SpecialityViewModel());
        modelAndView.addObject(MODEL_NAME_ALL_FACULTIES, conversionService.convert(
                facultyService.allFaculties(), facultyTypeDecriptor, facultyViewModelTypeDecriptor));
        modelAndView.addObject(MODEL_NAME_PREVIOUS_PAGE, PagesUtil.getPreviousPageUrl(request));
        return modelAndView;
    }

    @RequestMapping(value = "submit", method = RequestMethod.POST)
    public ModelAndView submitRegistrationForm(@ModelAttribute(MODEL_NAME_SPECIALITY) SpecialityViewModel specialityViewModel, BindingResult bindingResult, Model model){
        specialityValidator.validate(specialityViewModel, bindingResult);
        if (bindingResult.hasErrors()){
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName(VIEW_NAME_FORM);
            modelAndView.addObject(MODEL_NAME_SPECIALITY, specialityViewModel);
            modelAndView.addObject(MODEL_NAME_ALL_FACULTIES, conversionService.convert(
                    facultyService.allFaculties(), facultyTypeDecriptor, facultyViewModelTypeDecriptor));
            return modelAndView;
        }
        else
        {
            specialtyService.addSpecialty(conversionService.convert(specialityViewModel, Specialty.class));
            return new ModelAndView("redirect:/list/speciality");
        }
    }

    @RequestMapping(value = "delete/{specialityId}", method = RequestMethod.GET)
    public String deleteSpeciality(@PathVariable int specialityId, Model model, HttpServletRequest request){
        try {
            specialtyService.deleteSpecialty(specialtyService.findById(specialityId));
        } catch (DataIntegrityViolationException e){
            model.addAttribute(MODEL_NAME_PREVIOUS_PAGE, PagesUtil.getPreviousPageUrl(request));
            return "errors/CanNotRemove";
        }
        return "redirect:/list/speciality";
    }

    @RequestMapping(value = "edit/{specialityId}", method = RequestMethod.GET)
    public ModelAndView editSpeciality(@PathVariable int specialityId, HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName(VIEW_NAME_FORM);
        modelAndView.addObject(conversionService.convert(specialtyService.findById(specialityId),
                SpecialityViewModel.class));
        modelAndView.addObject(MODEL_NAME_ALL_FACULTIES, conversionService.convert(
                facultyService.allFaculties(), facultyTypeDecriptor, facultyViewModelTypeDecriptor));
        modelAndView.addObject(MODEL_NAME_EDITED_ID, specialityId);
        modelAndView.addObject(MODEL_NAME_PREVIOUS_PAGE, PagesUtil.getPreviousPageUrl(request));

        return modelAndView;
    }

    @RequestMapping(value = "submit/{specialityId}", method = RequestMethod.POST)
    public String submitEdit(@PathVariable int specialityId, @ModelAttribute(MODEL_NAME_SPECIALITY) SpecialityViewModel specialityViewModel,
                             BindingResult bindingResult){
        specialityValidator.validate(specialityViewModel, bindingResult);
        if (bindingResult.hasErrors())
            return VIEW_NAME_FORM;
        else
        {
            specialtyService.update(specialityViewModel.getSpecialtyName(),
                    facultyService.findFaculty(specialityViewModel.getFacultyName()),specialityId);
            return "redirect:/list/speciality";
        }
    }
}
