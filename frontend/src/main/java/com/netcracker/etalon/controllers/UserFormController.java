package com.netcracker.etalon.controllers;

import com.netcracker.etalon.validators.UserValidator;
import com.netcracker.etalon.beans.UserViewModel;
import com.netcracker.etalon.enteties.User;
import com.netcracker.etalon.servicies.UserService;
import com.netcracker.etalon.utils.PagesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UserFormController {
    @Autowired
    private UserService userService;
    @Autowired
    private ConversionService conversionService;
    @Autowired
    UserValidator userValidator;

    @RequestMapping("profile")
    public String getProfile(){
        return "authenticated/PersonalArea";
    }

    @RequestMapping(value = "registration", method = RequestMethod.GET)
    public ModelAndView getRegistrationForm(HttpServletRequest httpServletRequest){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("RegistrationForm");
        modelAndView.addObject("userViewModel", new UserViewModel());
        modelAndView.addObject("previousPage", PagesUtil.getPreviousPageUrl(httpServletRequest));
        return modelAndView;
    }

    @RequestMapping(value = "submit/registration", method = RequestMethod.POST)
    public String submitRegistrationForm(@ModelAttribute("userViewModel") UserViewModel userViewModel,
                                         BindingResult bindingResult){
        userValidator.validate(userViewModel, bindingResult);

        if (bindingResult.hasErrors()){
            return "RegistrationForm";
        }
        else
        {
            userService.addUser(conversionService.convert(userViewModel, User.class));
            return "redirect:/login";
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error) {

        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid username or password");
        }

        model.setViewName("login");

        return model;
    }
}
