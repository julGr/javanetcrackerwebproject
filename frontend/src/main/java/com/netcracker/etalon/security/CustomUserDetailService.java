package com.netcracker.etalon.security;

import com.netcracker.etalon.servicies.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


import java.util.HashSet;
import java.util.Set;

public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {

        com.netcracker.etalon.enteties.User user = userService.getUserByEmail(username);

        if (user == null) {
            throw new UsernameNotFoundException("Username not found");
        }

        String userName = user.getEmail();
        String password = user.getUserPassword();

        GrantedAuthority grantedAuthority = new GrantedAuthority() {
            @Override
            public String getAuthority() {
                if(user.getRole().name().equals("admin"))
                    return "ROLE_admin";
                else
                    return "ROLE_student";
            }
        };

        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.add(grantedAuthority);

        UserDetails details = new User(userName, password,true,true,true,true, authorities);
        return details;
    }
}
