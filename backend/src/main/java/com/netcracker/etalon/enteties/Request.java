package com.netcracker.etalon.enteties;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "request")
public class Request {
    private int id;
    private String requestName;
    private Date dateTo;
    private Date dateFrom;
    private Specialty availableSpecialty;
    private Faculty availableFaculty;
    private Set<Student> students;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "request_name", nullable = false, length = 255)
    public String getRequestName() {
        return requestName;
    }

    public void setRequestName(String requestName) {
        this.requestName = requestName;
    }

    @Basic
    @Column(name = "date_to", nullable = false)
    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    @Basic
    @Column(name = "date_from", nullable = false)
    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    @ManyToOne
    @JoinColumn(name = "available_specialty", referencedColumnName = "id", nullable = false)
    public Specialty getAvailableSpecialty() {
        return availableSpecialty;
    }

    public void setAvailableSpecialty(Specialty availableSpecialty) {
        this.availableSpecialty = availableSpecialty;
    }

    @ManyToOne
    @JoinColumn(name = "available_faculty", referencedColumnName = "id", nullable = false)
    public Faculty getAvailableFaculty() {
        return availableFaculty;
    }

    public void setAvailableFaculty(Faculty availableFaculty) {
        this.availableFaculty = availableFaculty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Request request = (Request) o;

        if (id != request.id) return false;
        if (availableSpecialty != request.availableSpecialty) return false;
        if (availableFaculty != request.availableFaculty) return false;
        if (requestName != null ? !requestName.equals(request.requestName) : request.requestName != null) return false;
        if (dateTo != null ? !dateTo.equals(request.dateTo) : request.dateTo != null) return false;
        if (dateFrom != null ? !dateFrom.equals(request.dateFrom) : request.dateFrom != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (requestName != null ? requestName.hashCode() : 0);
        result = 31 * result + (dateTo != null ? dateTo.hashCode() : 0);
        result = 31 * result + (dateFrom != null ? dateFrom.hashCode() : 0);
        return result;
    }

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "requests")
    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }
}
