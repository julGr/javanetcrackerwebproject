package com.netcracker.etalon.enteties;

import javax.persistence.*;

@Entity
@Table(name = "faculty")
public class Faculty {
    private int id;
    private String facultyName;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "faculty_name", nullable = false, length = 255)
    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Faculty faculty = (Faculty) o;

        if (id != faculty.id) return false;
        if (facultyName != null ? !facultyName.equals(faculty.facultyName) : faculty.facultyName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (facultyName != null ? facultyName.hashCode() : 0);
        return result;
    }
}
