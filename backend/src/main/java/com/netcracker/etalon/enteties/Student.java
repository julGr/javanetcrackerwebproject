package com.netcracker.etalon.enteties;

import com.netcracker.etalon.enteties.Enums.IsBudgetEnum;
import com.netcracker.etalon.enteties.Enums.StatusEnum;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "student")
public class Student {
    private int id;
    private String fName;
    private String lName;
    private Specialty specialty;
    private Faculty faculty;
    private String stGroup;
    private IsBudgetEnum isBudget;
    private double averageScore;
    private StatusEnum status;
    private Set<Request> requests;
    private String practicePeriod;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "f_name", nullable = false, length = 20)
    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    @Basic
    @Column(name = "l_name", nullable = false, length = 20)
    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    @ManyToOne
    @JoinColumn(name = "specialty", referencedColumnName = "id", nullable = false)
    public Specialty getSpecialty() {
        return specialty;
    }

    public void setSpecialty(Specialty specialty) {
        this.specialty = specialty;
    }

    @ManyToOne
    @JoinColumn(name = "faculty", referencedColumnName = "id",nullable = false)
    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    @Basic
    @Column(name = "st_group", nullable = false, length = -1)
    public String getStGroup() {
        return stGroup;
    }

    public void setStGroup(String stGroup) {
        this.stGroup = stGroup;
    }

    @Basic

    @Column(name = "average_score", nullable = false, precision = 0)
    public double getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(double averageScore) {
        this.averageScore = averageScore;
    }

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "student_request",
            joinColumns = { @JoinColumn(name = "student", nullable = false, updatable = false)},
            inverseJoinColumns = { @JoinColumn(name = "request", nullable = false, updatable = false) }
    )
    public Set<Request> getRequests() {
        return requests;
    }

    public void setRequests(Set<Request> requests) {
        this.requests = requests;
    }

    @Basic
    @Column(name = "practice_period", nullable = true, length = 255)
    public String getPracticePeriod() {
        return practicePeriod;
    }

    public void setPracticePeriod(String practicePeriod) {
        this.practicePeriod = practicePeriod;
    }

    @Column(name = "is_budget")
    @Enumerated(EnumType.STRING)
    public IsBudgetEnum getIsBudget() {
        return isBudget;
    }

    public void setIsBudget(IsBudgetEnum isBudget) {
        this.isBudget = isBudget;
    }

    @Column(name = "st_status")
    @Enumerated(EnumType.STRING)
    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (id != student.id) return false;
        if (specialty != student.specialty) return false;
        if (faculty != student.faculty) return false;
        if (Double.compare(student.averageScore, averageScore) != 0) return false;
        if (fName != null ? !fName.equals(student.fName) : student.fName != null) return false;
        if (lName != null ? !lName.equals(student.lName) : student.lName != null) return false;
        if (stGroup != null ? !stGroup.equals(student.stGroup) : student.stGroup != null) return false;
        if (practicePeriod != null ? !practicePeriod.equals(student.practicePeriod) : student.practicePeriod != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + (fName != null ? fName.hashCode() : 0);
        result = 31 * result + (lName != null ? lName.hashCode() : 0);
        result = 31 * result + (stGroup != null ? stGroup.hashCode() : 0);
        temp = Double.doubleToLongBits(averageScore);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (practicePeriod != null ? practicePeriod.hashCode() : 0);
        return result;
    }

}
