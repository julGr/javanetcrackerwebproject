package com.netcracker.etalon.enteties;

import javax.persistence.*;

@Entity
@Table(name = "specialty")
public class Specialty {
    private int id;
    private String specialtyName;
    private Faculty faculty;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "specialty_name", nullable = false, length = 255)
    public String getSpecialtyName() {
        return specialtyName;
    }

    public void setSpecialtyName(String specialtyName) {
        this.specialtyName = specialtyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Specialty specialty = (Specialty) o;

        if (id != specialty.id) return false;
        if (faculty != specialty.faculty) return false;
        if (specialtyName != null ? !specialtyName.equals(specialty.specialtyName) : specialty.specialtyName != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (specialtyName != null ? specialtyName.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "faculty", referencedColumnName = "id", nullable = false)
    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

}
