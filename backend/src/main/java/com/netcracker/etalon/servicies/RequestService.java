package com.netcracker.etalon.servicies;

import com.netcracker.etalon.enteties.Faculty;
import com.netcracker.etalon.enteties.Request;
import com.netcracker.etalon.enteties.Specialty;

import java.util.Date;
import java.util.List;

public interface RequestService {
    Request addRequest(Request request);
    List<Request> allRequests();
    Request findRequest(String requestName);
    Request findById(int id);
    void deleteRequest(Request request);
    void update(String requestName, Date dateFrom, Date dateTo, Faculty availableFaculty, Specialty availableSpeciality, int id);

}
