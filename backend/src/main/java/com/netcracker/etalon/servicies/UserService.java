package com.netcracker.etalon.servicies;

import com.netcracker.etalon.enteties.User;

import java.util.List;

public interface UserService {
    User addUser(User user);
    User getUserByEmail(String userName);
}
