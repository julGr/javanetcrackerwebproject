package com.netcracker.etalon.servicies.impl;

import com.netcracker.etalon.enteties.Faculty;
import com.netcracker.etalon.enteties.Specialty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.netcracker.etalon.repository.SpecialtyRepository;
import com.netcracker.etalon.servicies.SpecialtyService;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class SpecialtyServiceImpl implements SpecialtyService{

    @Autowired
    private SpecialtyRepository specialtyRepository;

    public Specialty addSpecialty(Specialty specialty) {
        return specialtyRepository.save(specialty);
    }

    public void deleteSpecialty(Specialty specialty) {
        specialtyRepository.delete(specialty);
    }

    public List<Specialty> allSpecialties() {
        List<Specialty> resultSList = new ArrayList<Specialty>();
        for (Specialty s: specialtyRepository.findAll()){
            resultSList.add(s);
        }
        return resultSList;
    }

    public Specialty findSpecialty(String specialtyName) {
        return specialtyRepository.findBySpecialtyName(specialtyName);
    }

    @Override
    public Specialty findById(int id) {
        return specialtyRepository.findById(id);
    }

    @Override
    public void update(String specialityName, Faculty faculty, int id) {
        specialtyRepository.update(specialityName, faculty, id);
    }
}
