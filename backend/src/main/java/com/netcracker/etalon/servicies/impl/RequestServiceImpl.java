package com.netcracker.etalon.servicies.impl;

import com.netcracker.etalon.enteties.Faculty;
import com.netcracker.etalon.enteties.Request;
import com.netcracker.etalon.enteties.Specialty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.netcracker.etalon.repository.RequestRepository;
import com.netcracker.etalon.servicies.RequestService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Transactional
@Service
public class RequestServiceImpl implements RequestService{

    @Autowired
    private RequestRepository requestRepository;

    public Request addRequest(Request request) {
        return requestRepository.save(request);
    }

    public List<Request> allRequests() {
        List<Request> resultRList = new ArrayList<Request>();
        for(Request f: requestRepository.findAll()){
            resultRList.add(f);
        }
        return resultRList;
    }

    public Request findRequest(String requestName) {
        return  requestRepository.findByRequestName(requestName);
    }

    @Override
    public Request findById(int id) {
        return requestRepository.findById(id);
    }

    @Override
    public void deleteRequest(Request request) {
        requestRepository.delete(request);
    }

    @Override
    public void update(String requestName, Date dateFrom, Date dateTo, Faculty availableFaculty, Specialty availableSpeciality, int id) {
        requestRepository.update(requestName, dateFrom, dateTo, availableFaculty, availableSpeciality, id);
    }
}
