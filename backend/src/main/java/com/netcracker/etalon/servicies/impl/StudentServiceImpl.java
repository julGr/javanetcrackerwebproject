package com.netcracker.etalon.servicies.impl;

import com.netcracker.etalon.enteties.Enums.IsBudgetEnum;
import com.netcracker.etalon.enteties.Faculty;
import com.netcracker.etalon.enteties.Specialty;
import com.netcracker.etalon.enteties.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.netcracker.etalon.repository.StudentRepository;
import com.netcracker.etalon.servicies.StudentService;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    public Student addStudent(Student student) {
        return studentRepository.save(student);
    }

    public void deleteStudent(Student student) {
        studentRepository.delete(student);
    }

    public List<Student> allStudents() {
        List<Student> resultStList = new ArrayList<Student>();
        for (Student st: studentRepository.findAll())
            resultStList.add(st);
        return resultStList;
    }

    public List<Student> allStudentsOnFaculty(Faculty faculty) {
        return studentRepository.findAllByFaculty(faculty);
    }

    public List<Student> allStudentsOnSpecialty(Specialty specialty) {
        return studentRepository.findAllBySpecialty(specialty);
    }

    @Override
    public Student findById(int id) {
        return studentRepository.findById(id);
    }

    @Override
    public List<Student> findAllLimit(int limit, int offset) {
        return studentRepository.findAllLimit(limit, offset);
    }

    @Override
    public void update(String fName, String lName, Specialty specialty, String stGroup, IsBudgetEnum isBudget, double averageScore, int id) {
        studentRepository.update(fName, lName, specialty, stGroup, isBudget,averageScore, id);
    }
}
