package com.netcracker.etalon.servicies;

import com.netcracker.etalon.enteties.Enums.IsBudgetEnum;
import com.netcracker.etalon.enteties.Faculty;
import com.netcracker.etalon.enteties.Specialty;
import com.netcracker.etalon.enteties.Student;

import java.util.List;

public interface StudentService {
    Student addStudent(Student student);
    void deleteStudent(Student student);
    List<Student> allStudents();
    List<Student> allStudentsOnFaculty(Faculty faculty);
    List<Student> allStudentsOnSpecialty(Specialty specialty);
    Student findById(int id);
    List<Student> findAllLimit(int limit, int offset);
    void update(String fName, String lName, Specialty specialty, String stGroup, IsBudgetEnum isBudget, double averageScore, int id);
}
