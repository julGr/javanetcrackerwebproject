package com.netcracker.etalon.servicies;

import com.netcracker.etalon.enteties.Faculty;

import java.util.List;

public interface FacultyService {
    Faculty addFaculty(Faculty faculty);
    void deleteFaculty(Faculty faculty);
    List<Faculty> allFaculties();
    Faculty findFaculty(String facultyName);
    Faculty findById(int id);
    void updateFaculty(String facultyName, int id);
}
