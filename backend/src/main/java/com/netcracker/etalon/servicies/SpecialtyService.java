package com.netcracker.etalon.servicies;

import com.netcracker.etalon.enteties.Faculty;
import com.netcracker.etalon.enteties.Specialty;

import java.util.List;

public interface SpecialtyService {
    Specialty addSpecialty(Specialty specialty);
    void deleteSpecialty(Specialty specialty);
    List<Specialty> allSpecialties();
    Specialty findSpecialty(String specialtyName);
    Specialty findById(int id);
    void update(String specialityName, Faculty faculty, int id);
}
