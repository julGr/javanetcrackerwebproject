package com.netcracker.etalon.servicies.impl;

import com.netcracker.etalon.enteties.Enums.RoleEnum;
import com.netcracker.etalon.enteties.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.netcracker.etalon.repository.UserRepository;
import com.netcracker.etalon.servicies.UserService;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    public User addUser(User user) {
        return userRepository.save(user);
    }

    public User getUserByEmail(String userName) {
        return userRepository.findByEmail(userName);
    }

}
