package com.netcracker.etalon.servicies.impl;

import com.netcracker.etalon.enteties.Faculty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.netcracker.etalon.repository.FacultyRepository;
import com.netcracker.etalon.servicies.FacultyService;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class FacultyServiceImpl implements FacultyService {

    @Autowired
    private FacultyRepository facultyRepository;

    public Faculty addFaculty(Faculty faculty) {
        return facultyRepository.save(faculty);
    }

    public void deleteFaculty(Faculty faculty) {
        facultyRepository.delete(faculty);
    }

    public List<Faculty> allFaculties() {
        List<Faculty> resultFList = new ArrayList<Faculty>();
        for(Faculty f: facultyRepository.findAll()){
            resultFList.add(f);
        }
        return resultFList;
    }

    public Faculty findFaculty(String facultyName) {
        return facultyRepository.findByFacultyName(facultyName);
    }

    @Override
    public Faculty findById(int id) {
        return facultyRepository.findById(id);
    }

    @Override
    public void updateFaculty(String facultyName, int id) {
        facultyRepository.updateFaculty(facultyName, id);
    }
}
