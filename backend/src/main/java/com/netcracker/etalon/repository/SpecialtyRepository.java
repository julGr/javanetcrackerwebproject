package com.netcracker.etalon.repository;

import com.netcracker.etalon.enteties.Faculty;
import com.netcracker.etalon.enteties.Specialty;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SpecialtyRepository extends CrudRepository<Specialty, Integer>{
    @Query("select s from Specialty s where s.faculty = :faculty")
    List<Specialty> findAllByFaculty(@Param("faculty") Faculty faculty);

    @Query("select s from Specialty  s where lower(s.specialtyName) = lower(:name)")
    Specialty findBySpecialtyName(@Param("name") String name);

    Specialty findById(int id);

    @Modifying
    @Query("update Specialty s set s.specialtyName = ?1, s.faculty = ?2 where s.id = ?3")
    void update(String specialtyName, Faculty faculty, int id);
}
