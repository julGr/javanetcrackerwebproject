package com.netcracker.etalon.repository;

import com.netcracker.etalon.enteties.Faculty;
import com.netcracker.etalon.enteties.Request;
import com.netcracker.etalon.enteties.Specialty;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;

public interface RequestRepository extends CrudRepository<Request, Integer> {
    @Query("select r from Request r where lower(r.requestName) = lower(:name) ")
    Request findByRequestName(@Param("name") String name);
    Request findById(int id);
    @Modifying
    @Query("update Request r set r.requestName = ?1, r.dateFrom = ?2, r.dateTo = ?3, r.availableFaculty = ?4, r.availableSpecialty =?5 where r.id = ?6")
    void update(String requestName, Date dateFrom, Date dateTo, Faculty availableFaculty, Specialty availableSpeciality, int id);
}
