package com.netcracker.etalon.repository;

import com.netcracker.etalon.enteties.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer>{
    User findByEmail(String name);
}
