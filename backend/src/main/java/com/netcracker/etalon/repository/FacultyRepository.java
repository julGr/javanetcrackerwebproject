package com.netcracker.etalon.repository;

import com.netcracker.etalon.enteties.Faculty;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface FacultyRepository extends CrudRepository<Faculty, Integer> {
    @Query("select f from Faculty f where lower(f.facultyName) = lower(:name)")
    Faculty findByFacultyName(@Param("name") String facultyName);

    Faculty findById(int id);

    @Modifying
    @Query("update Faculty f set f.facultyName = ?1 where f.id = ?2")
    void updateFaculty(String facultyName, int id);
}
