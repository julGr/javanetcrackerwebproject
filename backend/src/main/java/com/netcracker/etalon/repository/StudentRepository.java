package com.netcracker.etalon.repository;

import com.netcracker.etalon.enteties.Enums.IsBudgetEnum;
import com.netcracker.etalon.enteties.Faculty;
import com.netcracker.etalon.enteties.Specialty;
import com.netcracker.etalon.enteties.Student;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StudentRepository extends CrudRepository<Student, Integer>{
    @Query("select s from Student s where lower(s.fName) = lower(:name)")
    Student findByFName(@Param("name") String name);

    @Query("select s from Student s where lower(s.lName) = lower(:name)")
    Student findByLName(@Param("name") String name);

    @Query("select s from Student s where s.stGroup = :name ")
    List<Student> findByStGroup(@Param("name") Long group);

    @Query("select s from Student s where s.faculty = :name")
    List<Student> findAllByFaculty(@Param("name") Faculty faculty);

    @Query("select s from Student s where s.specialty = :name")
    List<Student> findAllBySpecialty(@Param("name") Specialty specialty);

    Student findById(int id);

    @Query(value ="SELECT * FROM projectStudentsPractice.student LIMIT ?1 OFFSET ?2",
            nativeQuery = true)
    List<Student> findAllLimit(int limit, int offset);

    @Modifying
    @Query("update Student s set s.fName = ?1, s.lName = ?2, s.specialty = ?3, s.stGroup = ?4, s.isBudget = ?5, s.averageScore = ?6 where s.id = ?7")
    void update(String fName, String lName, Specialty specialty, String stGroup, IsBudgetEnum isBudget, double averageScore, int id);
}
