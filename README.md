# etalon2017
Проект по назначению студентов на практику.
Написан на языке Java с использованием фрейморка Spring MVC.

- Используется сборщик Maven.

- Среда разработки, в котором проходило создание, отлаживание и дебаг проекта - Intellij Idea

- Сервер - Tomcat server

- В качестве системы управления БД используется MySQL
